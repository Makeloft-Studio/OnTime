﻿#pragma warning disable CS0612 // Type or member is obsolete

using Ace;
using Ace.Replication;
using Ace.Serialization;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;

#if !PARSER
using OnTime.Extensions;
#endif

using static System.Net.DecompressionMethods;

namespace OnTime
{
	public static class Aides
	{
		public static KeepProfile KeepProfile = new();

		public static ReplicationProfile JsonReplicationProfile = new()
		{ SimplifyMaps = true, SimplifySets = true, AttachType = false, AttachId = false};

		public static string ToJson<T>(this T item) =>
			item.CreateSnapshot(JsonReplicationProfile).ToString(KeepProfile);

		public static async Task<string> Load(this string uri,
			bool skipEscape = false, Encoding encoding = default, bool allowAutoRedirect = false) =>
			await WebSyncer.Default.Load(uri, skipEscape, encoding, allowAutoRedirect);

		public static async Task<string> Load(this string uri, HttpContext context,
			bool skipEscape = false, Encoding encoding = default, bool allowAutoRedirect = false) =>
			await WebSyncer.Default.Load(uri, skipEscape, encoding, allowAutoRedirect, context);

		public static IEnumerable<Match> Matches(this Regex regex, string input, int startIndex, int length)
		{
			do
			{
				var match = regex.Match(input, startIndex, length);
				if (match == Match.Empty) yield break;
				yield return match;
				var offset = match.Index + match.Length;
				var delta = offset - startIndex;
				startIndex = offset;
				length -= delta;
			} while (true);
		}

		public static object GetPropertyValue(this object item, string path)
		{
			if (string.IsNullOrEmpty(path)) return item;
			var dotIndex = path.IndexOf(".");
			var isPath = dotIndex >= 0;
			var propertyName = isPath
				? path.Substring(0, dotIndex)
				: path
				;
			var value = item.GetType().GetProperty(propertyName).Is(out var property)
				? property.GetValue(item)
				: item
				;
			return isPath
				? value?.GetPropertyValue(path.Substring(dotIndex + 1))
				: value
				;
		}

		public static async Task<Location> GetActiveLocationByIpAddress()
		{
			var info = await $"https://ipinfo.io".ToUri().DownloadStringTaskAsync();
			var regex = new Regex(@".loc.:\s*?.(?<lat>\d+.\d+)[\s\S]*?,[\s\S]*?(?<lon>\d+.\d+).");
			var match = regex.Match(info);
			match.Groups["lat"].Value.TryParse(out double latilude);
			match.Groups["lon"].Value.TryParse(out double longitude);
			return new(latilude, longitude);
		}

		public static async Task<Location> GetActiveLocationOrDefaultBySystem()
		{
			try
			{
				return
					await Geolocation.GetLastKnownLocationAsync() ??
					await Geolocation.GetLocationAsync(new GeolocationRequest(GeolocationAccuracy.Lowest,
						TimeSpan.FromSeconds(3)));
			}
			catch
			{
				return default;
			}
		}
		public static async Task<Location> GetActiveLocationByIpAddressOrDefault()
		{
			try
			{
				return await GetActiveLocationByIpAddress();
			}
			catch
			{
				return default;
			}
		}

		public static async Task<Location> GetActiveLocationOrDefault() =>
			await GetActiveLocationByIpAddressOrDefault() ?? await GetActiveLocationOrDefaultBySystem();

		public static double CalculateDistance(
			this Location from,
			(double Latitude, double Longitude) till,
			DistanceUnits distanceUnits = DistanceUnits.Kilometers) =>
			CalculateDistance((from.Latitude, from.Longitude), till, distanceUnits);

		public static double CalculateDistance(
			(double Latitude, double Longitude) from,
			(double Latitude, double Longitude) till,
			DistanceUnits distanceUnits = DistanceUnits.Kilometers) =>
			Location.CalculateDistance(
				from.Latitude, till.Longitude,
				till.Latitude, till.Longitude,
				distanceUnits);

		public static BindingFlags GetNonStaticFieldFlags =
			BindingFlags.Static | BindingFlags.GetField | BindingFlags.NonPublic;

		public static BindingFlags SetNonStaticFieldFlags =
			BindingFlags.Static | BindingFlags.SetField | BindingFlags.NonPublic;

		public static object GetNonPublicStaticField(this Type type, string fieldName, object value) =>
			type.InvokeMember(fieldName, GetNonStaticFieldFlags, default, default, new[] {value});

		public static object SetNonPublicStaticField(this Type type, string fieldName, object value) =>
			type.InvokeMember(fieldName, SetNonStaticFieldFlags, default, default, new[] {value});

		public static Action<string, string> SetCookies { get; set; }
		public static Func<string, string> GetCookies { get; set; }
		public static Func<string[]> SystemFontFamiliesProvider { get; set; }

		public static Func<HttpClientHandler> HttpClientHandlerProvider;

		public static HttpClientHandler CreateCustomHttpHandler(bool allowAutoRedirect = false) => SkipSsl(new()
		{
			AutomaticDecompression = Deflate | GZip,
			AllowAutoRedirect = allowAutoRedirect,
		});

		public static Dictionary<string, string> CustomHeaders { get; } = new()
		{
			["User-Agent"] = "Mozilla/5.0 (Linux; Android 5.0)",
		};

		private static HttpClientHandler SkipSsl(HttpClientHandler handler)
		{
			handler.ServerCertificateCustomValidationCallback += (sender, certificate, chain, errors) => true;
			return handler;
		}

		public static async Task<byte[]> DownloadByteArrayTaskAsync(this Uri uri, bool allowAutoRedirect = false)
		{
			/*** important to use HttpClient instead WebClient or HttpWebRequest ***/
			/*** to avoid first-time delay on some devices (for example Android 7) ***/

			//using var handler = HttpClientHandlerProvider?.Invoke() ?? new HttpClientHandler();
			using var handler = CreateCustomHttpHandler(allowAutoRedirect);
			using var client = new HttpClient(handler) { Timeout = TimeSpan.FromSeconds(16) };
			CustomHeaders.ForEach(client.DefaultRequestHeaders.Add);
			var bytes = await client.GetByteArrayAsync(uri);
			return bytes;
		}
#if NETSTANDARD
		public static List<string> CloudflareProtectedHosts = new() { "gomeltrans.net" };

		public static async Task<string> DownloadStringTaskAsyncViaWebView(this Uri uri)
		{
			var pageCompletionSource = new TaskCompletionSource<string>();
			var ghostRack = App.Current.MainPage.To<AppView>().GetGhostRack();
			var webView = new WebView { Source = uri, Opacity = 0.0 };

			async void OnNavigated(object sender, WebNavigatedEventArgs args)
			{
				try
				{
					if (args.Result.IsNot(WebNavigationResult.Success))
						throw new HttpRequestException($"{args.Result}: can not access\n{uri}");
					for (int i = 0; i < 16; i++)
					{
						var page = await webView.EvaluateJavaScriptAsync("document.documentElement.outerHTML");
						if (page.IsNot() || page.Contains("Cloudflare"))
						{
							webView.Opacity = 0.20;
							await Task.Delay(2000);
							continue;
						}

						var style = System.Globalization.NumberStyles.AllowHexSpecifier;
						page = Regex.Replace(page, @"\\[Uu]([0-9A-Fa-f]{4})", m => char.ToString((char)ushort.Parse(m.Groups[1].Value, style)));
						page = Regex.Unescape(page);

						pageCompletionSource.SetResult(page);
						return;
					}

					throw new Exception($"Possible, access to '{uri}' has been forbidden by Cloudflare.");
				}
				catch (Exception exception)
				{
					pageCompletionSource.SetException(exception);
				}
			}

			try
			{
				webView.Navigated += OnNavigated;
				ghostRack.Children.Clear();
				ghostRack.Children.Add(webView);
				return await pageCompletionSource.Task;
			}
			finally
			{
				webView.IsVisible = false;
				webView.Navigated -= OnNavigated;
				//ghostRack.Children.Remove(webView); // cause WebView exception
			}
		}
#endif
		public static async Task<string> DownloadStringTaskAsync(this Uri uri, Encoding encoding = default, bool allowAutoRedirect = false)
		{
#if NETSTANDARD
			if (CloudflareProtectedHosts.Contains(uri.Host))
				return await uri.DownloadStringTaskAsyncViaWebView();
#endif
			var bytes = await uri.DownloadByteArrayTaskAsync(allowAutoRedirect);
			var content = (encoding ?? Encoding.UTF8).GetString(bytes, 0, bytes.Length);
			return content;
		}

		public static async Task<string> DownloadStringTaskAsync(this Uri uri, HttpClient client, StringContent body, Encoding encoding = default)
		{
			var response = await (body.Is() ? client.PostAsync(uri, body) : client.GetAsync(uri));
			var bytes = await response.Content.ReadAsByteArrayAsync();
			var content = (encoding ?? Encoding.UTF8).GetString(bytes, 0, bytes.Length);
			return content;
		}
	}

	public class HttpContext
	{
		public HttpClient Client { get; set; }
		public Func<Task<StringContent>> GetBody { get; set; }
	}
}