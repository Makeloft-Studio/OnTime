﻿using Ace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	class BorisovBusInfo : AParser
	{
		static (Regex Route, Regex Direction, Regex DirectionValueRegex, Regex Stop, Regex Timetable, Regex Cell) CompileRegexes() =>
		(
			/*lang=regex*/@"<div class=.emodal-title.>.*?№(?<key>.+?)</div>[\s\S]*?<a class=.emodal-close.>".ToRegex(),
			/*lang=regex*/@"<div id=.tabs-(?<key>.+?).><p><div class=.omsc-accordion.>[\s\S]*?</div></div></div>".ToRegex(),
			/*lang=regex*/@"<a href=.\S+/#tabs-(?<key>.+?). >(?<value>.+?)</a>".ToRegex(),
			/*lang=regex*/ @"<div class=.omsc-toggle-title.>(?<key>.+?)</div>[\s\S]*?</div></div>".ToRegex(),
			/*lang=regex*/@"<strong>(?<key>.+?)</strong>(?<value>.*?)</span>".ToRegex(),
			/*lang=regex*/@"(?<key>\d+):(?<value>\d+)".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex DirectionValueRegex, Regex Stop, Regex Timetable, Regex Cell) regexes;
		static (Regex Route, Regex Direction, Regex DirectionValueRegex, Regex Stop, Regex Timetable, Regex Cell) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		public override string GetRouteNumbersSource() =>
			$"{Host}/расписание-автобусов-борисова/расписание-городских-автобусов/расписание-по-маршрутам/";

		public override async Task<IEnumerable<Context>> GetRoutes() => (await GetRoutes(Regexes.Route))
			.Select(r => r.Value.Is("918 (№18А)") ? new Context(r.Key, "18А (№918)", r.Match) : r);

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var keyToValue = Regexes.Direction.Matches(route.Match.Value).Cast<Match>()
				.ToDictionary(m => m.Groups["key"].Value, m => m.Groups["value"].Value);

			var directions = Regexes.Direction.Matches(route.Match.Value).Cast<Match>()
				.Select(m => new Context
				(
					m.Groups["key"].Value.To(out var key),
					keyToValue.GetValue(key, fallbackValue: key),
					m
				));
			
			return await directions.ToAsync();
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.Match.Value).Cast<Match>()
			.Select(m => new Context(m.Groups["key"].Value, m.Groups["key"].Value, m))
			.ToAsync();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>> GetTimetables(Context route, Context direction, Context stop)
		{
			return await Regexes.Timetable.Matches(stop.Match.Value).Cast<Match>().Select(m =>
			{
				GetRowsAsync getRows = async () => await GetTimetable(Regexes.Cell, m.Groups["value"].Value).ToAsync();
				return (FixPeriod(m.Groups["key"].Value), getRows);
			}).ToAsync();
		}

		public new IEnumerable<(string, string)> GetTimetable(Regex regex, string page) => regex
			.Matches(page).Cast<Match>()
			.GroupBy(c => c.Groups["key"].Value)
			.Select(g => (g.Key, g.ToString(m => m.Groups["value"].Value)));

		protected override string FixPeriod(string value, StringComparison comparison = StringComparison.OrdinalIgnoreCase) =>

			value.StartsWith("Па прац", comparison) ? "Workdays" :
			value.StartsWith("Па вых", comparison) ? "Weekends" :

			value;
	}
}
