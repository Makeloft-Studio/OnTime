﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ace;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	public class Kraj : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cell) CompileRegexes() =>
		(
			/*lang=regex*/@"href=.+?/transport/(.+?).>№(?<key>.+?)\s".ToRegex(),
			/*lang=regex*/@"href=.(?<key>.+?).>№(?<num>.+?)\s(?<value>.*?)</a>".ToRegex(),
			/*lang=regex*/@"</i>\s+(?<key>.+)\s+\n+\s*</a>[\s\S]*?<div class=.panel panel-default.>".ToRegex(),
			/*lang=regex*/@"4 style=.(.+?).><span class=.(.+?).>(?<key>.+?)</span></h4>[\s\S]*?\S\s*?((?!<h4).)*".ToRegex(),
			/*lang=regex*/@"(?<key>\d+)<sup>(?<value>\d+)</sup></b>".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cell) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cell) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "city-bus",
			_ => throw new System.NotImplementedException()
		};

		public override string GetSourceKey() => $"{LocalityName}~{base.GetSourceKey()}";
		public override string GetRouteNumbersSource() => $"{Host}/{LocalityName}/transport/{Kind}-routes";

		public override async Task<IEnumerable<Context>> GetRoutes() => (await GetRoutes(Regexes.Route)).Distinct(r => r.Key);

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var page = await route.SourcePage.ToAsync();
			return Regexes.Direction.Matches(page).Cast<Match>()
				.Where(m => m.Groups["num"].Value == route.Match.Groups["key"].Value)
				.Select(m => new Context(m.Groups["key"].Value, m.Groups["value"].Value, m));
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction)
		{
			var page = await $"{Host}{direction.Key}".Load();
			return Regexes.Stop.Matches(page).Cast<Match>().Select(m => new Context(m.Groups["key"].Value, m.Groups["key"].Value, m));
		}

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>> GetTimetables(Context route, Context direction, Context stop)
		{
			var page = await stop.Match.Value.ToAsync();
			return Regexes.Timetable.Matches(page).Cast<Match>().Select(m =>
			{
				GetRowsAsync getRows = async () => await GetTimetable(Regexes.Cell, m.Value).ToAsync();
				return (FixPeriod(m.Groups["key"].Value), getRows);
			});
		}
	}
}
