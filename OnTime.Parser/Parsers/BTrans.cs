﻿using Ace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	public class BTrans : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cell, Regex DayMinutes) CompileRegexes() =>
		(
			/*lang=regex*/@"<a href=./\S+?/(?<key>.+?). title=.(.+?). class=.hexagon-link-content. itemprop=.url. >(?<value>.+?)</a>".ToRegex(),
			/*lang=regex*/@"<div id=.(?<key>.+?). class=.direction.>[\s]*<h2>(?<value>.+?)</h2>[\s\S]*?</ul>[\s\S]*?</div>".ToRegex(),
			/*lang=regex*/@"<a class=.stop-link.\s+itemprop=.url. title=.(.+?). href=./\S+?/\S+?/(?<key>.+?).>([\s\S]+?)<!--/ noindex -->(?<value>.+?)</a>".ToRegex(),
			/*lang=regex*/@"<div class=.timetable.>[\S\s]*?</div>[\s]*?</div>[\s]*?</div>[\s]*?</div>".ToRegex(),
			/*lang=regex*/@"<div class=.timetable-ceil-hour.>(?<key>\d+?)</div>\s*(?<value>[\s\S]*?</div>)\s*</div>".ToRegex(),
			/*lang=regex*/@"<div class=.timetable-ceil-day-header .*?.>(?<day>.+?)</div>\s*<div class=.timetable-ceil-day-minutes .+?.>(?<minutes>.*?)</div>".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cell, Regex DayMinutes) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cell, Regex DayMinutes) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "avtobus",
			Kinds.Trolleybus => "trollejbus",
			Kinds.Tram => "tramvaj",
			Kinds.Metro => "metro",
			_ => throw new NotImplementedException()
		};

		public override string GetRouteNumbersSource() => $"{Host}/{Kind}";
		public override async Task<IEnumerable<Context>> GetRoutes() => await GetRoutes(Regexes.Route);

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var routeKey = HttpUtility.UrlEncode(route.Key).ToLower();
			var page = await $"{Host}/{Kind}/{routeKey}".Load(true);
			return Regexes.Direction.Matches(page).Cast<Match>().Select(m =>
				new Context(Uri.UnescapeDataString(m.Groups["key"].Value), Uri.UnescapeDataString(m.Groups["value"].Value), m));
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.Match.Value).Cast<Match>().Select(ToContext).ToAsync();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop)
		{
			var routeKey = HttpUtility.UrlEncode(route.Key).ToLower();
			var page = await $"{Host}/{Kind}/{routeKey}/{stop.Key}".Load(true);
			var mt = Regexes.Timetable.Match(page);
			var cells = GetCells(Regexes.Cell, page, mt.Index, mt.Length).ToList();
			var dayToHourMinutes = new Dictionary<string, List<(string Hour, string Minutes)>>();
			foreach (var (hour, value) in cells)
			{
				var items = Regexes.DayMinutes.Matches(value).Cast<Match>()
					.Select(m => (Day: FixPeriod(m.Groups["day"].Value), Minutes: m.Groups["minutes"].Value)).ToList();
				foreach (var (day, minutes) in items)
				{
					if (!dayToHourMinutes.ContainsKey(day))
						dayToHourMinutes[day] = new List<(string Hour, string Minutes)>();
					if (!minutes.IsNullOrWhiteSpace())
						dayToHourMinutes[day].Add((hour, minutes));
				}
			}

			dayToHourMinutes
				.Where(p => p.Value.Count.Is(1) && p.Value[0] == ("0", "0"))
				.ToList()
				.ForEach(i => dayToHourMinutes.Remove(i.Key));

			return dayToHourMinutes.Select(p =>
			{
				GetRowsAsync getRows = async () =>
					await p.Value.AsEnumerable().ToAsync();
				return (p.Key, getRows);
			});
		}

		public IEnumerable<(string Key, string Value)> GetCells(Regex regex, string page, int startIndex, int length) =>
			regex.Matches(page, startIndex, length).Select(m => (m.Groups["key"].Value, m.Groups["value"].Value));
	}
}
