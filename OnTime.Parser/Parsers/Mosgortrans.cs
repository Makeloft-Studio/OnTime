﻿using Ace;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using OnTime.Parsers.Patterns;

using static System.DayOfWeek;

namespace OnTime.Parsers
{
	class Mosgortrans : AParser
	{
		static (Regex Hour, Regex Minute) CompileRegexes() =>
		(
			/*lang=regex*/@"hour.>(?<key>.+?)</span></td>(?<value>[\s\S]*?)</td>".ToRegex(),
			/*lang=regex*/@".minutes.\s*>(?<value>\d+)".ToRegex()
		);

		static (Regex Hour, Regex Minute) regexes;
		static (Regex Hour, Regex Minute) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "avto",
			Kinds.Tram => "tram",
			Kinds.Trolleybus => "trol",
			_ => throw new NotImplementedException()
		};


		static readonly char[] _separators = { '\n' };

		public override string GetRouteNumbersSource() => $"{Host}/pass3/request.ajax.php?list=ways&type={Kind}";

		public override async Task<IEnumerable<Context>> GetRoutes()
		{
			var page = await GetRouteNumbersSource().Load(false, Windows1251.Encoding);
			return page.Split().Where(r => (0 < r.Length && r.Length < 5) || _skipedRoutes.Contains(r).Not()).Select(v => new Context(v, v));
		}

		static readonly string[] _skipedRoutes = { "route", "stations", "streets", "", default };

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var page = await $"{Host}/pass3/request.ajax.php?list=days&type={Kind}&way={route.Key}".Load(false, Windows1251.Encoding);
			var days = page.Split(_separators, StringSplitOptions.RemoveEmptyEntries);
			var directions = new List<string>(2);
			foreach (var d in days)
			{
				page = await $"{Host}/pass3/request.ajax.php?list=directions&type={Kind}&way={route.Key}&date={d}".Load(false, Windows1251.Encoding);
				page.Split(_separators, StringSplitOptions.RemoveEmptyEntries).ForEach(directions.Add);
			}

			var i = 0;
			return directions.Distinct().Select(v => new Context(i++%2 is 0 ? "AB" : "BA", v) { SourceData = days });
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction)
		{
			var days = (string[])direction.SourceData;
			var stopsInfo = new Dictionary<string, Dictionary<string, int>>();
			foreach (var day in days)
			{
				var page = await $"{Host}/pass3/request.ajax.php?list=waypoints&type={Kind}&way={route.Key}&date={day}&direction={direction.Key}".Load(false, Windows1251.Encoding);
				var stops = page.Split(_separators, StringSplitOptions.RemoveEmptyEntries);
				var i = 0;
				foreach (var stop in stops)
				{
					if (stopsInfo.Keys.Contains(stop).Not())
						stopsInfo[stop] = new Dictionary<string, int>();

					stopsInfo[stop][day] = i++;
				}
			}

			return stopsInfo.Keys.Select(v => new Context(v, v) { SourceData = stopsInfo[v] });
		}

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>> GetTimetables(Context route, Context direction, Context stop)
		{
			var days = (string[])direction.SourceData;
			var fullDays =
				days.Length.Is(0) ? new[] { "1111111" } :
				days.Length.Is(1) && days[0].IsNot("1111111") ? new[] { days[0], days[0].Replace('0', '~').Replace('1', '0').Replace('~', '1') } :
				days;

			var stopInfo = (Dictionary<string, int>)stop.SourceData;
			return await fullDays.Select(day =>
			{
				GetRowsAsync getRows = async () =>
				{
					if (stopInfo.ContainsKey(day).Not())
						return Enumerable.Empty<(string, string)>();

					var routeKey = route.Key.Aggregate("", (b, c) => b + (char.IsLetter(c) && c > 'Z' ? $"%{202 + (c - 'К'):X}" : c.ToString()));
					var uri = $"{Host}/pass3/shedule.php?type={Kind}&way={routeKey}&date={day}&direction={direction.Key}&waypoint={stopInfo[day]}";
					var page = await uri.Load(false, Windows1251.Encoding);
					var hours = Regexes.Hour.Matches(page).Cast<Match>();
					return hours
						.Select(h =>
						(
							Key: h.Groups["key"].Value,
							Value: Regexes.Minute.Matches(h.Groups["value"].Value).Cast<Match>().ToString(m => m.Groups["value"].Value))
						)
						.Where(l => l.Value.IsNotNullOrWhiteSpace());
				};
				return (FixPeriod(day), getRows);
			}).ToAsync();
		}

		protected override string FixPeriod(string value, StringComparison comparison = StringComparison.OrdinalIgnoreCase) => value switch
		{
			"1111100" => "Workdays",
			"0000011" => "Weekends",
			"1111111" => "Everyday",
			"1000000" => nameof(Monday),
			"0100000" => nameof(Tuesday),
			"0010000" => nameof(Wednesday),
			"0001000" => nameof(Thursday),
			"0000100" => nameof(Friday),
			"0000010" => nameof(Saturday),
			"0000001" => nameof(Sunday),
			_ => Enumerable.Range(0, 7).Aggregate(new StringBuilder(),
				(b, i) => b.Append(value[i].Is('1') ? RussianShortNamedDays[i] : "").Append(" ")).ToString()
		};

		static readonly string[] RussianShortNamedDays = { "пн", "вт", "ср", "чт", "пт", "сб", "вс" };
	}
}
