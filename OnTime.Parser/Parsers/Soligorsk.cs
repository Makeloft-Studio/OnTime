﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ace;
using Ace.Replication.Models;
using Ace.Serialization;

namespace OnTime.Parsers
{
	class Soligorsk : Patterns.AParser
	{
		static Regex CellRegex = /*lang=regex*/@"(?<key>\d+):(?<value>\d+)".ToRegex();

		static readonly KeepProfile KeepProfile = new() { EscapeProfile = { AsciMode = true } };

		public override string GetRouteNumbersSource() => $"{Host}/files/bus_bot/json/direction.php";
		public override async Task<IEnumerable<Context>> GetRoutes()
		{
			var page = await GetRouteNumbersSource().Load();
			var data = page.ParseSnapshot(keepProfile: KeepProfile).MasterState;
			return data.To<Map>()["ost_list"].To<Set>()
				.Select(i => i.To<Map>()["name"].To<string>()).Distinct()
				.Select(v => new Context(v, v) { SourcePage = page, SourceData = data });
		}

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var data = await route.SourceData.ToAsync();
			return data.To<Map>()["ost_list"].To<Set>().Cast<Map>()
				.Where(m => m["name"].Is(route.Key))
				.Select(m => new Context(m["dir_id"].To<string>(), m["dir_title"].To<string>()) { SourceData = data });
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction)
		{
			var page = await $"{Host}/files/bus_bot/json/direction.php?dbid={direction.Key}".Load();
			var data = page.ParseSnapshot(keepProfile: KeepProfile).MasterState;
			return data.To<Map>()["ost_list"].To<Set>().Cast<Map>()
				.Select(m => new Context(m["ost_id"].To<string>(), m["name"].To<string>()));
		}

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>> GetTimetables(Context route, Context direction, Context stop)
		{
			var page = await $"{Host}/files/bus_bot/json/timetables2.php?id={stop.Key}&dir_id={direction.Key}".Load();
			var data = page.ParseSnapshot(keepProfile: KeepProfile).MasterState;
			return data
				.To<Map>()["data"]
				.To<Map>()[$"_{route.Key}"]
				.To<Map>()["directions"]
				.To<Map>()[$"_{direction.Key}"]
				.To<Map>()["days"]
				.To<Set>().Cast<Map>()
				.Select(m =>
				{
					GetRowsAsync getRows = async () => await GetTimetable(CellRegex, m["timelist"].To<string>()).ToAsync();
					return (FixPeriod(m["plan_content_name"].To<string>()), getRows);
				});
		}
	}
}
