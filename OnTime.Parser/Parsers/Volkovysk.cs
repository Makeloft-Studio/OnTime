﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Ace;

namespace OnTime.Parsers
{
	class Volkovysk : Patterns.AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cells, Regex Cell) CompileRegexes() =>
		(
			/*lang=regex*/@"<a href=./bus/(?<key>.*?).><i class=.+></i> <strong>Автобус (?<value>.*?)</strong></a>".ToRegex(),
			/*lang=regex*/@"</i> (?<key>.*?)</h2>([\s\S]+?)</span>".ToRegex(),
			/*lang=regex*/@"<li><a href=.(?<key>.*?).>(?<value>.*?)</a></li>".ToRegex(),
			/*lang=regex*/@"data-toggle=.tab.>(?<key>.*)</a></li>".ToRegex(),
			/*lang=regex*/@"<tbody>(.*?)</tbody>".ToRegex(),
			/*lang=regex*/@"(?<key>\d+):(?<value>\d+)".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cells, Regex Cell) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cells, Regex Cell) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		public override string GetRouteNumbersSource() => $"{Host}/{Kind}";
		public override async Task<IEnumerable<Context>> GetRoutes() => await GetRoutes(Regexes.Route);

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var page = await $"{Host}/{Kind}/{route.Key}".Load();
			return Regexes.Direction.Matches(page).Cast<Match>()
				.Select(m => new Context(m.Groups["key"].Value, m.Groups["key"].Value, m));
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.Match.Value).Cast<Match>().Select(ToContext).Distinct(c => c.Value).ToAsync();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop)
		{
			var dayKeys = Regexes.Timetable.Matches(direction.Match.Value).Cast<Match>().Select(m => m.Groups["key"].Value).ToList();
			var isEveryday = dayKeys.Count.Is(0);
			if (isEveryday) dayKeys.Add("Everyday");

			return await dayKeys.Select(d =>
			{
				GetRowsAsync getRows = async () =>
				{
					try
					{
						var uri =
							d.Is("выходные") ? $"{Host}{stop.Key}".Replace("budni", "vykhodnye") :
							d.Is("будни") ? $"{Host}{stop.Key}".Replace("vykhodnye", "budni") :
							$"{Host}{stop.Key}";
						var page = await uri.Load();
						var cells = Regexes.Cells.Match(page).Value;
						return await GetTimetable(Regexes.Cell, cells).ToAsync();
					}
					catch (HttpRequestException exception)
					{
						return exception.Message.Contains("404") ? Enumerable.Empty<(string, string)>() : throw exception;
					}
				};

				return (Key: d, getRows);
			}).ToAsync();
		}
	}
}
