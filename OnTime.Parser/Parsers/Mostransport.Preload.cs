﻿using Ace;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;

namespace OnTime.Parsers
{
	partial class Mostransport
	{
		static readonly Regex RouteRegex =
			/*lang=regex*/@"href=./transport/schedule/route/(?<key>.+?).>[\s]*?<div class=.ts-number.>[\s]*?<i class=.ic ic.*?-(?<kind>.+?).></i>[\s]*(?<value>.+?)[\s]*</div>[\s]*?<div class=.ts-title.>[\s]*?<span>[\s]*(?<direction>.+?)[\s]*</span>[\s]*?</div>".ToRegex();

		static string GetKey(string[] l) => $"{l[0]}\t{l[1]}\t{l[2]}";
		static string GetShortKey(string[] l) => $"{l[1]}\t{l[2]}";
		static string GetDirection(string[] l) => l[3];

		static string GetArray(List<string> list) => list.ToString(s => s, "\t");
		public static async void Preload(string host)
		{
			var handler = new HttpClientHandler
			{
				AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate
			};

			var client = new HttpClient(handler);
			CustomHeaders.ForEach(client.DefaultRequestHeaders.Add);
			var context = new HttpContext { Client = client };

			var urls = Enumerable
				.Range(1, 24)
				.Select(i => $"{host}/ru/ajax/App/ScheduleController/getRoutesList" +
					$"?mgt_schedule[direction]={0}&page={i}")
				.ToList();

			var urlsA = Enumerable
				.Range(1, 24)
				.Select(i => $"{host}/ru/ajax/App/ScheduleController/getRoutesList" +
					$"?mgt_schedule[direction]={1}&page={i}")
				.ToArray();

			urls.AddRange(urlsA);

			var lines = new List<string>();
			foreach (var url in urls)
			{
				var page = await url.Load(context);
				foreach(var g in RouteRegex.Matches(page).Cast<Match>().Select(m => m.Groups))
				{
					var line = $"{g["kind"]}\t{g["value"]}\t{g["key"]}\t{g["direction"].Value}".Replace("&quot;", "\"");
					lines.Add(line);
				}
			}

			var rows = lines//File.ReadLines(@"C:\Users\Makeman\Desktop\directions.txt")
				.Select(l => l.Split('\t'))
				.Select(s => (GetKey(s), GetDirection(s)) ).ToArray();

			var result = new List<(string, List<string>)>();
			foreach (var row in rows)
			{
				if (result.FirstOrDefault(x => x.Item1 == row.Item1).To(out var t).IsNot(default))
				{
					t.Item2.Add(row.Item2);
				}
				else
				{
					result.Add((row.Item1, new List<string>(2) { row.Item2 }));
				}
			}

			File.WriteAllLines(@"C:\Users\Makeman\Desktop\bus.txt", result
				.Where(r => r.Item1.StartsWith("bus"))
				.Select(r => $"{GetShortKey(r.Item1.Split())}\t{r.Item2.ToString(s => s, "|")}"));

			File.WriteAllLines(@"C:\Users\Makeman\Desktop\tram.txt", result
				.Where(r => r.Item1.StartsWith("tram"))
				.Select(r => $"{GetShortKey(r.Item1.Split())}\t{r.Item2.ToString(s => s, "|")}"));

			File.WriteAllLines(@"C:\Users\Makeman\Desktop\trolleybus.txt", result
				.Where(r => r.Item1.StartsWith("trol"))
				.Select(r => $"{GetShortKey(r.Item1.Split())}\t{r.Item2.ToString(s => s, "|")}"));
		}
	}
}
