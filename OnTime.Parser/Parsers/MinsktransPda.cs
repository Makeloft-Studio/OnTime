﻿using Ace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	class MinsktransPda : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Week) CompileRegexes() =>
		(
			/*lang=regex*/@"<a href='.RouteNum=(?<key>.+?)&day=.+?&Transport=(.+?)'>(?<value_>.+?)</a>".ToRegex(),
			/*lang=regex*/@"<b>(?<key>[^<>]+?)</b><br>	&nbsp;&nbsp;.*?</a><br><br>".ToRegex(),
			/*lang=regex*/@"<a href='.RouteNum=(?<key1>.+?)&StopID=(?<key>.+?)&RouteType=(?<routeType>.+?)&day=\d+?&Transport=(.+?)'>(?<value>.+?)</a>".ToRegex(),
			/*lang=regex*/@"<br><b>(?<key>.+?)</b>.:.(?<value>(( <u>.+?</u>)|( \d+))*)".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Week) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Week) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "Autobus",
			Kinds.Trolleybus => "Trolleybus",
			Kinds.Tram => "Tramway",
			_ => throw new NotImplementedException()
		};

		public override string GetRouteNumbersSource() => $"{Host}/?Transport={Kind}&m=1&day=3";

		public override async Task<IEnumerable<Context>> GetRoutes()
		{
			var pageA = await $"{Host}/?Transport={Kind}&m=1&day=3".Load();
			var pageB = await $"{Host}/?Transport={Kind}&m=1&day=6".Load();
			var routesA = GetRoutes(pageA, Regexes.Route);
			var routesB = GetRoutes(pageB, Regexes.Route);
			return routesA.Union(routesB, Context.DefaultKeyComparer).OrderBy(x => x.Key, RouteNumberComparer.Default);
		}

		class RouteNumberComparer : IComparer<string>
		{
			public static readonly RouteNumberComparer Default = new RouteNumberComparer();

			public int Compare(string a, string b)
			{
				a = char.IsDigit(a[a.Length - 1]) ? a + "0" : a;
				b = char.IsDigit(b[b.Length - 1]) ? b + "0" : b;

				var dx = 4 - a.Length;
				var dy = 4 - b.Length;

				a = dx > 0 ? Prefixes[dx % Prefixes.Length] + a : a;
				b = dy > 0 ? Prefixes[dy % Prefixes.Length] + b : b;

				return string.Compare(a, b);
			}

			private static readonly string[] Prefixes = new[] { "", "0", "00", "000" };
		}

		class ContextComparer : IEqualityComparer<Context>
		{
			public bool Equals(Context x, Context y) => Equals(x.Key, y.Key);

			public int GetHashCode(Context obj) => obj.GetHashCode();
		}

		public override async Task<IEnumerable<Context>> GetDirections(Context c)
		{
			var pageA = await $"{Host}/?RouteNum={c.Key}&day=3&Transport={Kind}".Load();
			var pageB = await $"{Host}/?RouteNum={c.Key}&day=6&Transport={Kind}".Load();
			var directionsA = Regexes.Direction.Matches(pageA).Cast<Match>().Select(ToContext);
			var directionsB = Regexes.Direction.Matches(pageB).Cast<Match>().Select(ToContext);
			return directionsA.Union(directionsB, Context.DefaultKeyComparer);
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.Match.Value).Cast<Match>().Select(ToContext).Distinct(c => c.Value).ToAsync();

		readonly int[] weekRange = Enumerable.Range(1, 7).ToArray();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop) =>
			await weekRange.Select(i =>
			{
				GetRowsAsync getRows = async () =>
				{
					var routeType = stop.Match.Groups["routeType"];
					var page = await $"{Host}/?RouteNum={route.Key}&StopID={stop.Key}&RouteType={routeType}&day={i}&Transport={Kind}".Load();
					return GetTimetable(Regexes.Week, page, 0, page.Length);
				};
				return (Key: NumToDay(i).ToString(), GetRows: getRows);
			}).ToAsync();

		protected static DayOfWeek NumToDay(int i) => (DayOfWeek)(i % 7);

		public IEnumerable<(string, string)> GetTimetable(Regex regex, string page, int startIndex, int length) => regex
			.Matches(page, startIndex, length).Cast<Match>()
			.Select(m => (m.Groups["key"].Value, m.Groups["value"].Value.Replace("<u>", "").Replace("</u>", "")));
	}
}
