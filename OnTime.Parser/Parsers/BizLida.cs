﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Ace;

using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	public class BizLida : AParser
	{
		static (Regex Route, Regex Stop, Regex Cell) CompileRegexes() =>
		(
			/*lang=regex*/@"<a href=./.+?/(?<key>.+?)/[\s\S]*?<span class=.__number.>(?<value>.+?)</span>".ToRegex(),
			/*lang=regex*/@"<a href=./bus/.+?/.+?/.+?/(?<key>.+?).>\s*(?<value>.+?)\s*</a>".ToRegex(),
			/*lang=regex*/@"<div class=.bus-schedule-time\s*.>\s*(?<key>\d+):(?<value>\d+)\s*</div>".ToRegex()
		);

		static (Regex Route, Regex Stop, Regex Cell) regexes;
		static (Regex Route, Regex Stop, Regex Cell) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		public override string GetRouteNumbersSource() => $"{Host}/{Kind}";
		public override Task<IEnumerable<Context>> GetRoutes() => GetRoutes(Regexes.Route);

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var page = route.SourcePage;
			var directionValueRegex = @"<div class=.__route.>\s*(?<value>.+?)\s*</div>".ToRegex();

			var direction0LinkRegex = /*lang=regex*/$@"<span class=.__label.>\s*?(?<value>.+?)</span></a><a href=./.+?/{route.Key}/(?<key>.+?)/now.".ToRegex();
			var direction0LinkMatch = direction0LinkRegex.Match(page);
			var direction0Key = direction0LinkMatch.Groups["key"].Value;

			var direction0Page = await $"{Host}/{Kind}/{route.Key}/{direction0Key}/now".Load();
			var direction0Value = directionValueRegex.Match(direction0Page).Groups["value"].Value;
			//yield return new Context(direction0Key, direction0Value) { SourcePage = direction0Page };

			var direction1LinkRegex = /*lang=regex*/$@"<a href=./.+?/{route.Key}/(?<key>.+?)/now.".ToRegex();
			var direction1LinkMatch = direction1LinkRegex.Match(direction0Page);
			var direction1Key = direction1LinkMatch.Groups["key"].Value;

			var direction1Page = await $"{Host}/{Kind}/{route.Key}/{direction1Key}/now".Load();
			var direction1Value = directionValueRegex.Match(direction1Page).Groups["value"].Value;
			//yield return new Context(direction1Key, direction1Value) { SourcePage = direction1Page };

			return new Context[]
			{
				new Context(direction0Key, direction0Value) { SourcePage = direction0Page },
				new Context(direction1Key, direction1Value) { SourcePage = direction1Page },
			};
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.SourcePage).Cast<Match>().Select(ToContext).ToAsync();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>> GetTimetables(Context route, Context direction, Context stop) =>
			new string[] { "workday", "weekend" }.Select(d =>
			{
				GetRowsAsync getRows = async () =>
				{
					var page = await $"{Host}/{Kind}/{route.Key}/{direction.Key}/{d}/{stop.Key}".Load();
					return GetTimetable(Regexes.Cell, page);
				};

				return (FixPeriod(d), getRows);
			});

		private static string FixPeriod(string value) => value is "workday" ? "Workdays" : "Weekends";
	}
}
