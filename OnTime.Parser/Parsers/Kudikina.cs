﻿using Ace;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	class Kudikina : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Cell) CompileRegexes() =>
		(
			/*lang=regex*/@"href=./.+?/.+?/(?<key>.+?)""[\s\S]\s+ title=.\S+? (?<value>.+?),".ToRegex(),
			/*lang=regex*/@"<li class=.*>[\s\S]+?<a href=./.+?/.+?/.+?/(?<key>(.+?)(?<!online|ignore_too)).>[\s\S]+?\s*\S+?\s+(?<value>.+?)\s*?</a>[\s\S]+?</li>".ToRegex(),
			/*lang=regex*/@"<div class=.row.>[\s\S]+?<div class=.bus-stop.+?.>[\s\S]+?(<span.*>.+?</span>)*?[\s\S]+?<a href=./.+?/(?<key>.+?).>\s*\S+?\s(?<value>.+?)\s*</a>[\s\S]+?</div>[\s\S]+?<hr/>".ToRegex(),
			/*lang=regex*/@"(?<key>\d+):(?<value>\d+)".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Cell) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Cell) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "bus",
			Kinds.Tram => "tram",
			Kinds.Trolleybus => "trolley",
			Kinds.Minibus => "mtaxi",
			_ => throw new NotImplementedException()
		};

		protected string locality;
		public string Locality => locality ??= LocalityNameToLocalityCode
			.FirstOrDefault(p => p.Name.Is(LocalityName, StringComparison.OrdinalIgnoreCase)).Code;

		public static (string Name, string Code)[] LocalityNameToLocalityCode =
		{
			("Moscow", "msk"),
			("Vologda", "vlgd"),
			("Yekaterinburg", "ekb"),
			("Kazan", "kzn"),
			("Kaliningrad", "klgd"),
			("Kirov", "kirov"),
			("Krasnodar", "krd"),
			("Magnitogorsk", "mgn"),
			("Murmansk", "murma"),
			("Nizhniy Novgorod", "nnov"),
			("Novorossiysk", "nvrsk"),
			("Novosibirsk", "nsk"),
			("Perm", "perm"),
			("Pskov", "pskov"),
			("Rostov-on-Don", "rnd"),
			("Smolensk", "smol"),
			("Khabarovsk", "hab"),
			("Chelyabinsk", "chel"),
			("Yaroslavl", "yar"),

			("Balashikha", "balas"),
			("Barybino", "barib"),
			("Vereya", "verey"),
			("Vidnoye", "vidno"),
			("Volokolamsk", "volok"),
			("Voskresensk", "voskr"),
			("Dmitrov", "dmitr"),
			("Dolgoprudny", "dolgo"),
			("Domodedovo", "domod"),
			("Yegoryevsk", "egori"),
			("Zhukovsky", "zhuko"),
			("Zvenigorod", "zveni"),
			("Ivanteyevka", "ivant"),
			("Istra", "istra"),
			("Kashira", "kashi"),
			("Klin", "klin"),
			("Kolomna", "kolom"),
			("Korolyov", "korol"),
			("Kurovskoe", "kurov"),
			("Losino-Petrovsky", "losin"),
			("Lukhovitsy", "lukho"),
			("Lytkarino", "litka"),
			("Lyubertsy", "luber"),
			("Malino", "malin"),
			("Mozhaysk", "mozha"),
			("Mytischi", "mitis"),
			("Naro-Fominsk", "narof"),
			("Noginsk", "nogin"),
			("Odintsovo", "odinc"),
			("Omsk", "omsk"),
			("Ozyory", "ozeri"),
			("Orekhovo-Zuyevo", "orekh"),
			("Pavlovskiy Posad", "pavlo"),
			("Podolsk", "podol"),
			("Ramenskoye", "ramen"),
			("Roshal", "rosha"),
			("Ruza", "ruza"),
			("Sergiyev Posad", "sergi"),
			("Serebryanyye Prudy", "sereb"),
			("Serpukhov", "serpu"),
			("Solnechnogorsk", "solne"),
			("Stupino", "stupi"),
			("Taldom", "taldo"),
			("Fryanovo", "fryno"),
			("Khimki", "khimk"),
			("Chekhov", "chekh"),
			("Shatura", "shatu"),
			("Shakhovskaya", "shaho"),
			("Schyolkovo", "schel"),
			("Elektrostal", "elekt"),

			("Volgograd", "vlg"),
			("Volzhsky", "volzh"),
			("Dubovka", "dubov"),
			("Kamyshin", "kamis"),
			("Krasnoslobodsk", "krass"),
			("Novoanninsky", "novoa"),
			("Uryupinsk", "uryup"),
			("Frolovo", "frolo"),
			("Frolovskiy", "frolov"),

			("Kemerovo", "kem"),
			("Anzhero-Sudzhensk", "ansuj"),
			("Belovo", "belov"),
			("Beryozovskiy", "berez"),
			("Izhmorskiy", "izhmo"),
			("Kiselyovsk", "kisel"),
			("Krapivinskiy", "krapi"),
			("Leninsk-Kuznetskiy", "lenku"),
			("Mariinsk", "marii"),
			("Mezhdurechensk", "mejre"),
			("Myski", "miski"),
			("Osinniki", "osinn"),
			("Prokopyevsk", "proko"),
			("Promyshlennovskiy", "promi"),
			("Tayga", "taiga"),
			("Tashtagol", "tasta"),
			("Tisulskiy", "tisul"),
			("Topki", "topki"),
			("Tyazhinskiy", "tajin"),
			("Yurga", "yurga"),
			("Yashkinskiy", "yashk"),

			("Tobolsk", "tob"),
			("Zavodoukovsk", "zavuk"),
			("Ishim", "ishim"),
			("Yalutorovsk", "yalut"),

			("Achinsk", "achin"),
			("Zheleznogorsk", "zhel"),
			("Uyar", "uyar"),

			("Saki", "saki"),
			("Sevastopol", "sev"),
			("Simferopol", "sim"),
			("Kerch", "kerch"),
		};

		public override string GetSourceKey() => $"{Locality}~{base.GetSourceKey()}";

		public async override Task<IEnumerable<Context>> GetRoutes()
		{
			var page = await $"{Host}/{Locality}/{Kind}/".Load();
			return Regexes.Route.Matches(page).Cast<Match>().Select(m => new Context(m));
		}

		public async override Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var page = await $"{Host}/{Locality}/{Kind}/{route.Key}/A".Load();
			return Regexes.Direction.Matches(page).Cast<Match>().Select(m => new Context(m));
		}

		static readonly string[] Days = { "1", "2", "3", "4", "5", "6", "0" };

		public async override Task<IEnumerable<Context>> GetStops(Context route, Context direction)
		{
			var stopsInfo = new Dictionary<string, Dictionary<string, int>>();
			foreach (var day in Days)
			{
				var page = await $"{Host}/{Locality}/{Kind}/{route.Key}/{direction.Key}?d={day}".Load();
				var stops = Regexes.Stop.Matches(page).Cast<Match>().Select(m => m.Groups["value"].Value);
				var i = 0;
				foreach (var stop in stops)
				{
					if (stopsInfo.Keys.Contains(stop).Not())
						stopsInfo[stop] = new Dictionary<string, int>();

					stopsInfo[stop][day] = i++;
				}
			}

			return stopsInfo.Keys.Select(v => new Context(v, v) { SourceData = stopsInfo[v] });
		}

		public async override Task<IEnumerable<(string Key, GetRowsAsync GetRows)>> GetTimetables(Context route, Context direction, Context stop)
		{
			var stopInfo = (Dictionary<string, int>)stop.SourceData;
			return await Days.Select(day =>
			{
				GetRowsAsync getRows = async () =>
				{
					if (stopInfo.ContainsKey(day).Not())
						return Enumerable.Empty<(string, string)>();

					var page = await $"{Host}/{Locality}/{Kind}/{route.Key}/{direction.Key}?d={day}".Load();
					var stops = Regexes.Stop.Matches(page).Cast<Match>();
					var stopMatch = stops.FirstOrDefault(s => s.Groups["value"].Value.Is(stop.Value));
					var cells = Regexes.Cell.Matches(stopMatch.Value).Cast<Match>().ToList();
					var hours = cells.Select(m => m.Groups["key"].Value).Distinct().ToList();
					return hours
						.Select(h =>
						(
							Key: h,
							Value: cells.Where(m => m.Groups["key"].Value.Is(h)).Cast<Match>().ToString(m => m.Groups["value"].Value))
						)
						.Where(l => l.Value.IsNotNullOrWhiteSpace());
				};
				return (FixPeriod(day), getRows);
			}).ToAsync();
		}

		protected override string FixPeriod(string value, StringComparison comparison = StringComparison.OrdinalIgnoreCase) =>
			((DayOfWeek)int.Parse(value)).ToString();
	}
}
