﻿using Ace;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.DayOfWeek;

public delegate Task<IEnumerable<(string Key, string Value)>> GetRowsAsync();

namespace OnTime.Parsers.Patterns
{
	public enum Kinds { Bus, Trolleybus, Tram, Metro, Minibus }

	public abstract class AParser
	{
		public class Context : Dictionary<string, object>
		{
			public Context(string key) => Key = key.Replace("&quot;", "\"");
			public Context(string key, string value) : this(key) => Value = value.Replace("&quot;", "\"");
			public Context(string key, string value, Match match) : this(key, value) => Match = match;
			public Context(Match match) : this(match.Groups["key"].Value, match.Groups["value"].Value) => Match = match;

			public string Key { get; private set; }
			public string Value { get; private set; }
			public Match Match { get; private set; }

			public string SourcePage { get; set; }
			public object SourceData { get; set; }

			public static readonly KeyComparer DefaultKeyComparer = new();

			public class KeyComparer : IEqualityComparer<Context>
			{
				public bool Equals(Context x, Context y) => Equals(x.Key, y.Key);

				public int GetHashCode(Context obj) => obj.Key.GetHashCode();
			}
		}

		public void Setup(string host, string locality, Kinds kind)
		{
			Host = host;
			LocalityName = locality;
			OriginalKind = kind;
			Kind = GetKind(kind);
		}

		public string LocalityName { get; set; }
		public virtual string Host { get; set; }
		public virtual string Kind { get; private set; }

		public Kinds OriginalKind { get; protected set; }

		protected virtual string GetKind(Kinds kind) => kind.ToString().ToLower();

		public virtual string GetSourceKey() => Host
			.Replace("http://www.", "").Replace("https://www.", "")
			.Replace("http://", "").Replace("https://", "");

		public virtual string GetRouteNumbersSource() => default;

		public abstract Task<IEnumerable<Context>> GetRoutes();
		protected virtual async Task<IEnumerable<Context>> GetRoutes(Regex regex)
		{
			var page = await GetRouteNumbersSource().Load();
			return GetRoutes(page, regex);
		}

		protected virtual IEnumerable<Context> GetRoutes(string page, Regex regex) =>
			regex.Matches(page).Cast<Match>().Select(m => new Context
			(
				Uri.UnescapeDataString(m.Groups["key"].Value.To(out var key)),
				Uri.UnescapeDataString(m.Groups["value"].Value.To(out var value).IsNullOrEmpty() ? key : value),
				m
			) { SourcePage = page });

		public virtual Context ToContext(Match m) => new
		(
			m.Groups["key"].Value,
			(m.Groups["value"].Value is string value && value.Length > 0
				? value
				: m.Groups["key"].Value).Replace("'", "\""),
			m
		);

		public abstract Task<IEnumerable<Context>> GetDirections(Context route);
		public abstract Task<IEnumerable<Context>> GetStops(Context route, Context direction);

		public abstract Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop);

		public virtual IEnumerable<(string Hour, string Minutes)> GetTimetable(Regex regex, string page) => regex
			.Matches(page).Cast<Match>()
			.GroupBy(c => c.Groups["key"].Value)
			.Select(g => (g.Key, g.ToString(m => m.Groups["value"].Value)));

		protected virtual string FixPeriod(string v, StringComparison с = StringComparison.OrdinalIgnoreCase) =>
			
			v.StartsWith("ра", с) ? "Workdays" :
			v.StartsWith("бу", с) ? "Workdays" :
			v.StartsWith("вы", с) ? "Weekends" :
			v.StartsWith("еж", с) ? "Everyday" :

			v.Is("пн", с) || v.StartsWith("по", с) ? nameof(Monday) :
			v.Is("вт", с) || v.StartsWith("вт", с) ? nameof(Tuesday) :
			v.Is("ср", с) || v.StartsWith("ср", с) ? nameof(Wednesday) :
			v.Is("чт", с) || v.StartsWith("че", с) ? nameof(Thursday) :
			v.Is("пт", с) || v.StartsWith("пя", с) ? nameof(Friday) :
			v.Is("сб", с) || v.StartsWith("су", с) ? nameof(Saturday) :
			v.Is("вс", с) || v.StartsWith("во", с) ? nameof(Sunday) :

			v;

		public async Task Write(int skipRoutesCount = 0)
		{
			var _ = "•";
			var routes = await GetRoutes();
			var fileName = $"{LocalityName}#{Kind.ToLower()}.txt";
			if (skipRoutesCount == 0) File.Delete(fileName);
			using (var stringWriter = File.AppendText(fileName))

			foreach (var route in routes.Skip(skipRoutesCount))
			{
				stringWriter.WriteLine($"\t\t\t\t{route.Value}");
				Console.WriteLine($"\t\t\t\t{route.Key}{_}{route.Value}");
				var directions = await GetDirections(route);
				foreach (var direction in directions)
				{
					stringWriter.WriteLine($"\t\t\t{direction.Key}{_}{direction.Value}");
					Console.WriteLine($"\t\t\t{direction.Key}{_}{direction.Value}");
					var stops = await GetStops(route, direction);
					foreach (var stop in stops)
					{
						stringWriter.WriteLine($"\t\t{stop.Key}{_}{stop.Value}");
						Console.WriteLine($"\t\t{stop.Key}{_}{stop.Value}");
						var timetables = await GetTimetables(route, direction, stop);
						foreach (var timetable in timetables)
						{
							stringWriter.WriteLine($"\t{timetable.Key}");
							Console.WriteLine($"\t{timetable.Key}");
							var rows = await timetable.GetRows();
							foreach (var row in rows)
							{
								stringWriter.WriteLine($"{row.Key}{_}{row.Value}");
								Console.WriteLine($"{row.Key}{_}{row.Value}");
							}
						}
					}
				}
			}
		}
	}
}
