﻿using Ace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	class Pinskap : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cell, Regex DirectionName, Regex Days, Regex Day) CompileRegexes() =>
		(
			/*lang=regex*/@"<a class=.rasp-title. href=.raspisanie/gorodskie-marshrutyi/(?<key>.+?).>[\s\S]*?<span class=.ui-nomer.>(?<value>.+?)</span>[\s\S]*?<h4>(?<description>.+?)</h4>".ToRegex(),
			/*lang=regex*/@"<div class=.(?<key>rasp.*?)"".*?>[\s\S]*?\s+</div>".ToRegex(),
			/*lang=regex*/@"<section class=.accordion_item.><h3 class=.title_block.>(?<key>.*?)</h3>[\s\S]*?</section>".ToRegex(),
			/*lang=regex*/@"<table style=.float: left.>\s*?<thead>[\S\s]*?<th style=.width: 220px.>(?<key>.+?)</th>[\S\s]*?</table>".ToRegex(),
			/*lang=regex*/@"data-time=.\s*?(?<key>\d+):(?<value>\d+)\s*?.".ToRegex(),
			/*lang=regex*/@"<h1 class=.pagetitle.>[\s\S]*?-\s*(?<value>.*)[\s\S]*?</h1>".ToRegex(),
			/*lang=regex*/@"<ul class=.tabs__caption.>(?<days>[\s\S]*?)</ul>".ToRegex(),
			/*lang=regex*/@"<li.*?>(?<key>.*?)</li>".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cell, Regex DirectionName, Regex Days, Regex Day) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Cell, Regex DirectionName, Regex Days, Regex Day) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => "gorodskie-marshrutyi";

		public override string GetRouteNumbersSource() => $"{Host}/raspisanie/{Kind}/";

		protected override async Task<IEnumerable<Context>> GetRoutes(Regex regex)
		{
			var page = await GetRouteNumbersSource().Load();
			return regex.Matches(page).Cast<Match>().Select(m =>
				new Context(Uri.UnescapeDataString(m.Groups["key"].Value), Uri.UnescapeDataString(m.Groups["value"].Value)));
		}

		public override Task<IEnumerable<Context>> GetRoutes() => GetRoutes(Regexes.Route);

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var page = await $"{Host}/raspisanie/{Kind}/{route.Key}".Load();
			var matches = Regexes.Direction.Matches(page).Cast<Match>().ToList();
			var isNotRound = matches.Any(m => m.Groups["key"].Value.Is("rasp obr"));
			var directionBaseName = Regexes.DirectionName.Match(page).Groups["value"].Value;
			return matches.Select(m =>
			{
				var isDirect = m.Groups["key"].Value.Is("rasp");
				var name = isDirect ? $">> {directionBaseName} >>" : $"<< {directionBaseName} <<";
				return new Context($"{m.Groups["key"].Value}", name, m)	{ SourcePage = page };
			}).Distinct(d => d.Key);
		}

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction)
		{
			var page = await direction.SourcePage.ToAsync();
			var matches = Regexes.Direction.Matches(page).Cast<Match>().ToList();
			var match = matches.FirstOrDefault(m => m.Groups["key"].Value.Is(direction.Key));
			return Regexes.Stop.Matches(match.Value).Cast<Match>().Select(ToContext);
		}

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop)
		{
			var page = await direction.SourcePage.ToAsync();
			var daysMatch = Regexes.Days.Match(page);
			var days = Regexes.Day.Matches(daysMatch.Value).Cast<Match>().Select(m => m.Groups["key"].Value).ToList();
			var i = 0;
			return Regexes.Direction.Matches(page).Cast<Match>().Where(m => m.Groups["key"].Value.Is(direction.Key)).Select(m =>
			{
				var stopMatch = Regexes.Stop
					.Matches(m.Value).Cast<Match>()
					.FirstOrDefault(s => s.Groups["key"].Value.Is(stop.Key)) ?? Match.Empty;
				GetRowsAsync getRows = async () => await GetTimetable(Regexes.Cell, stopMatch.Value).ToAsync();
				return (FixPeriod(days[i++]), getRows);
			});
		}
	}
}
