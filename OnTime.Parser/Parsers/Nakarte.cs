﻿using Ace;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	public class Nakarte : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Location) CompileRegexes() =>
		(
			/*lang=regex*/@"<a href=..+?/\S+/(?<key>.+?).>\s*<span class=.number.>(?<value>.+?)</span>".ToRegex(),
			/*lang=regex*/@"<div class=.stops.>\s*<h2>(?<value>.*?)</h2>\s*?(.*?)\s*?</div>".ToRegex(),
			/*lang=regex*/@"<a href=..+?/.+?/.+?/.+?/.+?/(?<key>.+?).>(?<value>.*?)</a>".ToRegex(),
			/*lang=regex*/@"const (?<key>lat|lon) = (?<value>\d+.\d+)".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Location) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Location) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "avtobus",
			Kinds.Trolleybus => "trolleybus",
			Kinds.Tram => "tramvay",
			Kinds.Metro => "metro",
			_ => throw new NotImplementedException()
		};

		public override string GetRouteNumbersSource() => $"{Host}/{Kind}";

		public override Task<IEnumerable<Context>> GetRoutes() => GetRoutes(Regexes.Route);

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.Match.Value).Cast<Match>().Select(ToContext).Distinct(c => c.Value).ToAsync();

		public override async Task<IEnumerable<Context>> GetDirections(Context c)
		{
			var page = await $"{Host}/{Kind}/{c.Key}".Load();
			return Regexes.Direction.Matches(page).Cast<Match>().Select(ToContext);
		}

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop)
		{
			try
			{
				var page = await $"{Host}/{Kind}/{route.Key}/{stop.Key}".Load();
				var matches = Regexes.Location.Matches(page);
				return matches.Cast<Match>().Select(m =>
				{
					GetRowsAsync getRows = async () =>
						await Task.FromResult(Enumerable.Empty<(string, string)>());
					return (m.Groups["value"].Value, getRows);
				});
			}
			catch
			{
				return Enumerable.Empty<(string, GetRowsAsync GetRows)>();
			}
			//var mt = TimetableRegex.Match(page);
			//var cells = GetCells(CellRegex, page, mt.Index, mt.Length).ToList();
			//var daysToHourMinutes = new Dictionary<string, List<(string Key, string Value)>>();
			//foreach (var cell in cells)
			//{
			//	var dayMinutes = DayMinutesRegex.Matches(cell.Value)
			//		.Select(m => (m.Groups["days"].Value, m.Groups["minutes"].Value)).ToList();
			//	foreach (var dayMinute in dayMinutes)
			//	{
			//		if (!daysToHourMinutes.ContainsKey(dayMinute.Item1))
			//			daysToHourMinutes[dayMinute.Item1] = new List<(string Key, string Value)>();
			//		daysToHourMinutes[dayMinute.Item1].Add((cell.Key, dayMinute.Item2));
			//	}
			//}

			//return daysToHourMinutes.Select(p => (p.Key, p.Value.AsEnumerable()));
		}

		public IEnumerable<(string Key, string Value)> GetCells(Regex regex, string page, int startIndex, int length) =>
			regex.Matches(page, startIndex, length).Select(m =>
			(m.Groups["key"].Value, m.Groups["value"].Value));
	}
}
