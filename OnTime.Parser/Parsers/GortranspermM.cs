﻿using Ace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	public class GortranspermM : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Hour, Regex Minute) CompileRegexes() =>
		(
			/*lang=regex*/@"<li>[\s\S]*?<a href=./route/(?<key>.+?)/.>[\n\t\s]*(?<value>.+?),[\s\S]*?</a>[\n\t\s]*</li>".ToRegex(),
			/*lang=regex*/@"<div data-role=..+?.>[\n\t\s]*<h3>[\n\t\s]*(?<key>.+?)[\n\t\s]*</h3>[\s\S]*?</div>".ToRegex(),
			/*lang=regex*/@"<li>[\s\S]*?<a href=./time-table/.+?/(?<key>.+?).>[\n\t\s]*(?<value>.+?)[\n\t\s]*</a>[\n\t\s]*</li>".ToRegex(),
			/*lang=regex*/@"<li>[\n\t\s]*<div class=.hour.>[\n\t\s]*(?<key>\d+)[\n\t\s]*</div>(?<value>[\s\S]*?)</li>".ToRegex(),
			/*lang=regex*/@"\d+".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Hour, Regex Minute) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Hour, Regex Minute) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => kind switch
		{
			Kinds.Bus => "0",
			Kinds.Trolleybus => "1",
			Kinds.Tram => "2",
			Kinds.Minibus => "3",
			_ => throw new NotImplementedException()
		};

		public override string GetRouteNumbersSource() => $"{Host}/routes-list/{Kind}/";
		public override async Task<IEnumerable<Context>> GetRoutes() => await GetRoutes(Regexes.Route);

		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var page = await $"{Host}/route/{route.Key}/".Load();
			return Regexes.Direction.Matches(Adapt(page)).Cast<Match>().Select(ToContext);
		}

		private string Adapt(string text) => text.Replace("&#034;", "\"");

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction) =>
			await Regexes.Stop.Matches(direction.Match.Value).Cast<Match>().Select(ToContext).ToAsync();

		readonly int[] weekRange = Enumerable.Range(0, 7).ToArray();

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop)
		{
			var today = Clock.GetTimetableDate();

			return await weekRange.Select(i =>
			{
				var day = today.AddDays(i);
				var dayName = day.DayOfWeek.ToString();

				GetRowsAsync getRows = async () =>
				{
					var page = await $"{Host}/time-table/{route.Key}/{stop.Key}?date={day:dd.MM.yyyy}".Load();
					return GetTimetable(Regexes.Hour, page);
				};
				return (Key: dayName, GetRows: getRows);
			}).ToAsync();
		}

		public override IEnumerable<(string Hour, string Minutes)> GetTimetable(Regex regex, string page) =>
			regex.Matches(page).Cast<Match>().Select(m =>
			(
				m.Groups["key"].Value,
				Regexes.Minute.Matches(m.Groups["value"].Value).Cast<Match>().ToString(m => m.Value)
			));
	}
}
