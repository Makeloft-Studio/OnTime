﻿using Ace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

namespace OnTime.Parsers
{
	public class Barautopark : AParser
	{
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Week) CompileRegexes() =>
		(
			/*lang=regex*/@"/services/freight/\d+/.>.\s*?(?<key>\d+).*?\s*?(?<value_>.+)</a></td>".ToRegex(),
			/*lang=regex*/@"/services/freight/(?<key>\d+)/.>(?<value>.+)</a></td>".ToRegex(),
			/*lang=regex*/@"id=.(?<key>.+?). data-type-path=.forward.><a href=.#d\d+.>(?<value>.+?)</a></div>".ToRegex(),
			/*lang=regex*/@"<table style=.float: left.>\s*?<thead>[\S\s]*?<th style=.(.*?).>(?<key>.+?)</th>[\S\s]*?</table>".ToRegex(),
			/*lang=regex*/@"<td style=.(.*?).><span>(?<key>.+)</span></td>[\n\r]+.+<td style=.(.*?).>(?<value>.+)</td>".ToRegex()
		);

		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Week) regexes;
		static (Regex Route, Regex Direction, Regex Stop, Regex Timetable, Regex Week) Regexes =>
			regexes.Is(default) ? regexes = CompileRegexes() : regexes;

		protected override string GetKind(Kinds kind) => "freight";

		public string Mode { get; set; }

		public override string GetSourceKey() => Mode.Is("?print=y") ? $"print~{base.GetSourceKey()}" : base.GetSourceKey();

		public override string GetRouteNumbersSource() => $"{Host}/services/{Kind}/5/{Mode}";
		public override async Task<IEnumerable<Context>> GetRoutes() => (await GetRoutes(Regexes.Route)).Distinct(r => r.Key);

		private static readonly char[] _separators = new[] { '№', ' ', '-' };
		public override async Task<IEnumerable<Context>> GetDirections(Context route)
		{
			var page = await route.SourcePage.ToAsync();
			return Regexes.Direction.Matches(page).Cast<Match>()
				.Where(m => m.Groups["value"].Value.SplitByChars("№ -")[0] == route.Key)
				.Select(m => new Context(m.Groups["key"].Value, CorrectDirectionValue(m.Groups["value"].Value, route.Key), m));
		}

		private string CorrectDirectionValue(string directionValue, string key) =>
			directionValue.Replace($"№{key} ", "").To(out var value).StartsWith("№")
			? value
				.Replace($"№{key}-т ", "")
				.Replace($"№{key}-T ", "")
				.Replace($"№ {key}-т ", "")
				.Replace($"№ {key}-T ", "")
				.Replace($"№{key}-", "")
			: value;

		public override async Task<IEnumerable<Context>> GetStops(Context route, Context direction)
		{
			var page = await $"{Host}/services/{Kind}/{direction.Key}/".Load();
			return Regexes.Stop.Matches(page).Cast<Match>().Select(m => new Context(m.Groups["key"].Value, m.Groups["value"].Value));
		}

		public override async Task<IEnumerable<(string Key, GetRowsAsync GetRows)>>
			GetTimetables(Context route, Context direction, Context stop)
		{
			var page = await $"{Host}/bitrix/templates/barautopark/ajax.php?action=getBusPath&element_id={stop.Key}&path_type=forward".Load();
			return Regexes.Timetable.Matches(page).Cast<Match>().Select(m =>
			{
				GetRowsAsync getRows = async () => await GetTimetable(Regexes.Week, m.Value).ToAsync();
				return (FixPeriod(m.Groups["key"].Value), getRows);
			});
		}

		protected override string FixPeriod(string value, StringComparison comparison = StringComparison.OrdinalIgnoreCase) =>
			value.Is("понедельник - пятница", comparison) ? "Workdays" : base.FixPeriod(value, comparison);

		public override IEnumerable<(string Hour, string Minutes)> GetTimetable(Regex regex, string page) =>
			regex.Matches(page).Cast<Match>().Select(m => (m.Groups["key"].Value, UnglueMinutes(m.Groups["value"].Value)));

		private static string UnglueMinutes(string minutes)
		{
			while (true)
			{
				var result = Regex.Replace(minutes, /*lang=regex*/@"(?<before>.*? *?\d+)(?<glued>\d\d)(?<after> *?.*?)", @"${before} ${glued}${after}");
				if (result == minutes) return result;
				minutes = result;
			}
		}
	}
}
