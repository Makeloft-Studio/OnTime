﻿using Ace;
using System.Linq;
using OnTime.Parsers;
using OnTime.Parsers.Patterns;

using static OnTime.Parsers.Patterns.Kinds;

namespace OnTime
{
	public static class Data
	{
		public static (string Key, AParser[] Parsers, bool Status)[] GetSources(string region) => region switch
        {
            "Belarus" => GetSourcesBelarus(),
            "Ukrain" => GetSourcesUkrain(),
            "Russia" => GetSourcesRussia(),
            "MoscowRegion" => GetSourcesMoscowRegion(),
            "VolgogradRegion" => GetSourcesVolgogradRegion(),
            "KemerovoRegion" => GetSourcesKemerovoRegion(),
            "KrasnoyarskRegion" => GetSourcesKrasnoyarskRegion(),
            "TyumenRegion" => GetSourcesTyumenRegion(),
            "Crimea" => GetSourcesCrimea(),

            _ => throw new System.NotImplementedException(region)
        };

		public static (string Key, AParser[] Parsers, bool Status)[] GetSourcesMoscowRegion() => new[]
		{
			Create<Kudikina>("Moscow", Bus, Tram),
			Create<Mostransport>("Moscow", Bus, Tram),

			Create<Kudikina>("Balashikha", Bus),
			Create<Kudikina>("Barybino", Bus),
			Create<Kudikina>("Vereya", Bus),
			Create<Kudikina>("Vidnoye", Bus),
			Create<Kudikina>("Volokolamsk", Bus),
			Create<Kudikina>("Voskresensk", Bus),
			Create<Kudikina>("Dmitrov", Bus),
			Create<Kudikina>("Dolgoprudny", Bus),
			Create<Kudikina>("Domodedovo", Bus),
			Create<Kudikina>("Yegoryevsk", Bus),
			Create<Kudikina>("Zhukovsky", Bus),
			Create<Kudikina>("Zvenigorod", Bus),
			Create<Kudikina>("Ivanteyevka", Bus),
			Create<Kudikina>("Istra", Bus),
			Create<Kudikina>("Kashira", Bus),
			Create<Kudikina>("Klin", Bus),
			Create<Kudikina>("Kolomna", Bus),
			Create<Kudikina>("Korolyov", Bus),
			Create<Kudikina>("Kurovskoe", Bus),
			Create<Kudikina>("Losino-Petrovsky", Bus),
			Create<Kudikina>("Lukhovitsy", Bus),
			Create<Kudikina>("Lytkarino", Bus),
			Create<Kudikina>("Lyubertsy", Bus),
			Create<Kudikina>("Malino", Bus),
			Create<Kudikina>("Mozhaysk", Bus),
			Create<Kudikina>("Mytischi", Bus),
			Create<Kudikina>("Naro-Fominsk", Bus),
			Create<Kudikina>("Noginsk", Bus),
			Create<Kudikina>("Odintsovo", Bus),
			Create<Kudikina>("Ozyory", Bus),
			Create<Kudikina>("Orekhovo-Zuyevo", Bus),
			Create<Kudikina>("Pavlovskiy Posad", Bus),
			Create<Kudikina>("Podolsk", Bus),
			Create<Kudikina>("Ramenskoye", Bus),
			Create<Kudikina>("Roshal", Bus),
			Create<Kudikina>("Ruza", Bus),
			Create<Kudikina>("Sergiyev Posad", Bus),
			Create<Kudikina>("Serebryanyye Prudy", Bus),
			Create<Kudikina>("Serpukhov", Bus),
			Create<Kudikina>("Solnechnogorsk", Bus),
			Create<Kudikina>("Stupino", Bus),
			Create<Kudikina>("Taldom", Bus),
			Create<Kudikina>("Fryanovo", Bus),
			Create<Kudikina>("Khimki", Bus),
			Create<Kudikina>("Chekhov", Bus),
			Create<Kudikina>("Shatura", Bus),
			Create<Kudikina>("Shakhovskaya", Bus),
			Create<Kudikina>("Schyolkovo", Bus),
			Create<Kudikina>("Elektrostal", Bus),
		};

		public static (string Key, AParser[] Parsers, bool Status)[] GetSourcesVolgogradRegion() => new[]
		{
			Create<Kudikina>("Volgograd", Bus, Trolleybus, Tram),

			Create<Kudikina>("Volzhsky", Bus),
			Create<Kudikina>("Dubovka", Bus),
			Create<Kudikina>("Kamyshin", Bus),
			Create<Kudikina>("Krasnoslobodsk", Bus),
			Create<Kudikina>("Novoanninsky", Bus),
			Create<Kudikina>("Uryupinsk", Bus),
			Create<Kudikina>("Frolovo", Bus),
			Create<Kudikina>("Frolovskiy", Bus),
		};

		public static (string Key, AParser[] Parsers, bool Status)[] GetSourcesKemerovoRegion() => new[]
		{
			Create<Kudikina>("Kemerovo", Bus),

			Create<Kudikina>("Anzhero-Sudzhensk", Bus),
			Create<Kudikina>("Belovo", Bus),
			Create<Kudikina>("Beryozovskiy", Bus),
			Create<Kudikina>("Izhmorskiy", Bus),
			Create<Kudikina>("Kiselyovsk", Bus),
			Create<Kudikina>("Krapivinskiy", Bus),
			Create<Kudikina>("Leninsk-Kuznetskiy", Bus),
			Create<Kudikina>("Mariinsk", Bus),
			Create<Kudikina>("Mezhdurechensk", Bus),
			Create<Kudikina>("Myski", Bus),
			Create<Kudikina>("Osinniki", Bus),
			Create<Kudikina>("Prokopyevsk", Bus),
			Create<Kudikina>("Promyshlennovskiy", Bus),
			Create<Kudikina>("Tayga", Bus),
			Create<Kudikina>("Tashtagol", Bus),
			Create<Kudikina>("Tisulskiy", Bus),
			Create<Kudikina>("Topki", Bus),
			Create<Kudikina>("Tyazhinskiy", Bus),
			Create<Kudikina>("Yurga", Bus),
			Create<Kudikina>("Yashkinskiy", Bus),
		};

		public static (string Key, AParser[] Parsers, bool Status)[] GetSourcesTyumenRegion() => new[]
		{
			Create<Kudikina>("Tobolsk", Bus),
			Create<Kudikina>("Zavodoukovsk", Bus),
			Create<Kudikina>("Ishim", Bus),
			Create<Kudikina>("Yalutorovsk", Bus),
		};

		public static (string Key, AParser[] Parsers, bool Status)[] GetSourcesCrimea() => new[]
		{
			Create<Kudikina>("Saki", Bus),
			Create<Kudikina>("Sevastopol", Bus, Trolleybus),
			Create<Kudikina>("Simferopol", Bus, Trolleybus),
			Create<Kudikina>("Kerch", Bus),
		};

		public static (string Key, AParser[] Parsers, bool Status)[] GetSourcesKrasnoyarskRegion() => new[]
		{
			Create<Kudikina>("Achinsk", Bus),
			Create<Kudikina>("Zheleznogorsk", Bus),
			Create<Kudikina>("Uyar", Bus),
		};

		public static (string Key, AParser[] Parsers, bool Status)[] GetSourcesRussia() => new[]
		{
			Create<Mostransport>("Moscow", Bus, Tram, Trolleybus),
			Create<Kudikina>("Moscow", Bus, Tram),
			
			Create<Kudikina>("Vologda", Bus),
			Create<Kudikina>("Yekaterinburg", Bus, Trolleybus, Tram),
			Create<Kudikina>("Kazan", Bus, Trolleybus, Tram),
			Create<Kudikina>("Kaliningrad", Bus),
			Create<Kudikina>("Kirov", Bus, Trolleybus),
			Create<Kudikina>("Krasnodar", Bus, Trolleybus, Tram),
			Create<Kudikina>("Magnitogorsk", Tram),
			Create<Murmansktrolleybus>("Murmansk", Bus, Trolleybus),
			Create<Kudikina>("Murmansk", Bus, Trolleybus),
			Create<Kudikina>("Nizhniy Novgorod", Bus, Trolleybus, Tram),
			Create<Kudikina>("Novorossiysk", Bus, Trolleybus, Tram, Minibus),
			Create<Kudikina>("Novosibirsk", Bus, Trolleybus, Tram, Minibus),
			Create<Kudikina>("Omsk", Bus, Trolleybus, Tram, Minibus),
			Create<GortranspermM>("Perm", Bus, Tram, Minibus),
			Create<Kudikina>("Perm", Bus, Tram, Minibus),
			Create<Kudikina>("Pskov", Bus),
			Create<Kudikina>("Rostov-on-Don", Bus, Trolleybus, Tram),
			Create<Kudikina>("Smolensk", Bus, Trolleybus, Tram),
			Create<Kudikina>("Khabarovsk", Bus, Trolleybus, Tram),
			Create<Kudikina>("Chelyabinsk", Bus, Trolleybus, Tram),
			Create<Kudikina>("Yaroslavl", Bus),
		};

		public static (string Key, AParser[] Parsers, bool Status)[] GetSourcesUkrain() => new[]
		{
			Create<Kyivpastrans>("Kyiv", Bus, Tram, Trolleybus),
		};

		public static (string Key, AParser[] Parsers, bool Status)[] GetSourcesBelarus() => new[]
		{
			Create<Minsktrans>("Minsk", Bus, Trolleybus, Tram),
			Create<Gusts>("Minsk", Minibus),
			Create<TudaSuda>("Minsk", Bus, Trolleybus, Tram, Metro, Minibus),
			Create<Kogda>("Minsk", Bus, Trolleybus, Tram, Metro),
			Create<BTrans>("Minsk", Bus, Trolleybus, Tram),

			Create<Gomeltrans>("Gomel", Bus, Trolleybus),
			Create<TudaSuda>("Gomel", Bus, Trolleybus, Minibus),
			Create<Kogda>("Gomel", Bus, Trolleybus),
			Create<BTrans>("Gomel", Bus, Trolleybus),

			Create<Kogda>("Mogilev", Bus, Trolleybus),
			Create<BTrans>("Mogilev", Bus, Trolleybus),

			Create<Kogda>("Vitebsk", Bus, Trolleybus, Tram),
			Create<BTrans>("Vitebsk", Bus, Trolleybus, Tram),

			Create<Kogda>("Grodno", Bus, Trolleybus),
			Create<BTrans>("Grodno", Bus, Trolleybus),

			Create<TudaSuda>("Brest", Bus, Trolleybus, Minibus),
			Create<Kogda>("Brest", Bus, Trolleybus),
			Create<BTrans>("Brest", Bus, Trolleybus),

			Create<Kogda>("Bobruisk", Bus, Trolleybus),

			Create<Barautopark>("Baranovichi", Bus),
			Create<Kogda>("Baranovichi", Bus),
			Create<BTrans>("Baranovichi", Bus),

			Create<Pinskap>("Pinsk", Bus),
			Create<Kogda>("Pinsk", Bus),
			Create<BTrans>("Pinsk", Bus),

			Create<BTrans>("Orsha", Bus),

			Create<BTrans>("Lida", Bus),

			Create<Soligorsk>("Soligorsk", Bus),

			Create<Kraj>("Molodechno", Bus),
			Create<BTrans>("Molodechno", Bus),

			Create<BTrans>("Kobrin", Bus),

			Create<BTrans>("Volkovysk", Bus),
			Create<Volkovysk>("Volkovysk", Bus),

			Create<Kraj>("Vilejka", Bus),

			Create<Kraj>("Glubokoe", Bus),

			Create<Kraj>("Smorgon", Bus),

			Create<Kraj>("Mjadel", Bus),

			Create<Kraj>("Volozhin", Bus),

			Create<Kraj>("Postavy", Bus),
		};

		private static (string Locality, AParser[] Parsers, bool IsOfficial)
			Create<TParser>(string locality, params Kinds[] kinds) where TParser : AParser, new() =>
			(
				Locality: locality,
				Parsers: kinds.Select(k => New<TParser>(locality.ToLower(), k)).ToArray().To(out var parsers),
				IsOfficial: parsers.All(p => OfficialHosts.Contains(p.Host))
			);

		private static AParser New<TParser>(string locality, Kinds kind) where TParser : AParser, new()
		{
			var parser = new TParser();
			var host = GetDefaultHost(parser, locality);
			parser.Setup(host, locality, kind);
			return parser;
		}

		public readonly static string[] OfficialHosts =
		{
			"http://www.m.gortransperm.ru",
			"https://murmansktrolleybus.ru",
			"https://transport.mos.ru",
			"https://kpt.kyiv.ua",
			"https://minsktrans.by",
			"https://barautopark.by",
			"https://pinskap.by",
			"http://gusts.minsk.by",
		};

		public static string GetDefaultHost(AParser parser, string locality) => parser switch
		{
			Barautopark => "https://barautopark.by",
			BTrans => $"https://{locality}.btrans.by",
			//Busget => "https://busget.ru",
			Gomeltrans => "https://gomeltrans.net",
			GortranspermM => "http://www.m.gortransperm.ru",
			Gusts => $"http://gusts.{locality}.by",
			Kogda => "https://kogda.by",
			Kraj => "https://kraj.by",
			Kudikina => "https://kudikina.ru",
			Mostransport => "https://transport.mos.ru",
			Minsktrans => "https://minsktrans.by",
			Murmansktrolleybus => "https://murmansktrolleybus.ru",
			Pinskap => "https://pinskap.by",
			Soligorsk => "https://esoligorsk.by",
			TudaSuda => "https://www.tuda-suda.by",
			Volkovysk => "https://volkovysk.by",
			Kyivpastrans => "https://kpt.kyiv.ua",

			_ => throw new System.NotImplementedException()
		};
	}
}
