﻿using Ace;

using OnTime.ViewModels;

using static OnTime.LanguageCodes;
using static OnTime.Geodata;

namespace OnTime
{
	public class GeoMaps
	{
		private static readonly AppViewModel AppViewModel = Store.Get<AppViewModel>();
		private static readonly TimetableViewModel TimetableViewModel = Store.Get<TimetableViewModel>();
		private static readonly LocalizationSource DefaultLocalizationSource = new() { ActiveManager = new LanguageManager(Russian)};

		private static LocalizationSource GetLocalizationSource(LanguageCodes language) => language switch
		{
			Ukrainian => DefaultLocalizationSource,
			_ => LocalizationSource.Wrap
		};

		private static string GetDomain(LanguageCodes language) => language switch
		{
			English => "https://yandex.com/maps",
			Belorussian => "https://yandex.by/maps",
			_ => "https://yandex.ru/maps"
		};

		public static string GetActiveUri()
		{
			var locality = TimetableViewModel.Root?.ActiveItem;
			var kind = locality?.ActiveItem;
			var route = kind?.ActiveItem;

			var routeKey = route?.Value;
			var kindKey = kind?.Value;
			var (geoKey, geoCode) = locality.Is() && Catalog.TryGetValue(locality.Value, out var geo)
				? (geo.Key, geo.Code)
				: (DefaultGeoKey, DefaultGeoCode);

			var activeLanguage = AppViewModel.ActiveLanguage;
			var domain = GetDomain(activeLanguage);
			var localizer = GetLocalizationSource(activeLanguage);
			var searchQuery = $"{localizer[geoKey]} {localizer[kindKey]} {routeKey}".Trim();
			var url = $"{domain}/{geoCode}/search/{searchQuery}";
			return url;
		}
	}
}
