﻿using Ace;
using Ace.Replication;

using OnTime.Extensions;

using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Essentials;

#if WPF
using FontAttributes = System.Windows.FontWeights;
#else
using Xamarin.Forms;
#endif

using Yandex.Metrica;

using static OnTime.LanguageCodes;

namespace OnTime
{
	[DataContract]
	public class Link
	{
		[DataMember]
		public string Text { get; set; }
		[DataMember]
		public string Address { get; set; }
	}

	[DataContract]
	public class AppViewModel : ContextObject, IExposable
	{
		static readonly Link RefreshLink = new() { Text = "🔄", Address = "RefreshLinks" };
		public Link[] ActiveLinks { get; set; } = { RefreshLink };
		public Dictionary<string, Link[]> LanguageCodeToLinks { get; set; }

		public LanguageCodes[] Languages { get; private set; }
			= Enum.GetValues(TypeOf<LanguageCodes>.Raw).Cast<LanguageCodes>().ToArray();

		private readonly LanguageCodes _defaultLanguage = GetDefaultLanguage();

		public string[] Regions { get; } =
		{
			"Belarus",
			"Ukrain",
			"Russia",
			"MoscowRegion",
			"VolgogradRegion",
			"KemerovoRegion",
			"KrasnoyarskRegion",
			"TyumenRegion",
			"Crimea",
		};

		[DataMember]
		public string ActiveRegion
		{
			get => Get(() => ActiveRegion);
			set => Set(() => ActiveRegion, value);
		}

		[DataMember]
		public LanguageCodes ActiveLanguage
		{
			get => Get(() => ActiveLanguage, _defaultLanguage);
			set => Set(() => ActiveLanguage, value, true);
		}

		public CultureInfo ActiveCulture => new(ActiveLanguage switch
		{
			Belorussian => "be",
			Ukrainian => "uk",
			Russian => "ru",
			English => "en",
			_ => throw new NotImplementedException(ActiveLanguage.To<string>())
		});

		private bool _useTransliteration;
		[DataMember]
		public bool UseTransliteration
		{
			get => _useTransliteration;
			set => (_useTransliteration = value)
				.Call(() => EvokePropertyChanged(nameof(UseTransliteration)));
		}

		[DataMember]
		public string ActiveColorPalette
		{
			get => Get(() => ActiveColorPalette) ?? "Ocean";
			set => Set(() => ActiveColorPalette, value ?? ActiveColorPalette, true);
		}

		public SmartSet<string> ColorPalettes { get; private set; }

		[DataMember]
		public SmartSet<string> FontFamilies { get; set; }

		[DataMember]
		public FontInfo[] FontInfos { get; set; }
		[DataMember]
		public FontInfo ActiveFontInfo
		{
			get => Get(() => ActiveFontInfo) ?? FontInfos?.FirstOrDefault();
			set => Set(() => ActiveFontInfo, value ?? ActiveFontInfo, true);
		}

		[DataMember]
		public double Intensity
		{
			get => Get(() => Intensity, 0.08);
			set => Set(() => Intensity, value);
		}

		[DataMember]
		public string ColorFilter
		{
			get => Get(() => ColorFilter, "#000000");
			set => Set(() => ColorFilter, value);
		}

		[DataMember]
		public bool AllowBonus
		{
			get => Get(() => AllowBonus, false);
			set => Set(() => AllowBonus, value);
		}

		[DataMember]
		public bool AllowAdvertising
		{
			get => Get(() => AllowAdvertising, true);
			set => Set(() => AllowAdvertising, value);
		}

		public string AppVersionName => AppInfo.VersionString;
		public string AppBuildNumber => AppInfo.BuildString;

		readonly ReplicationProfile _replicationProfile = new()
		{
			AttachId = false,
			AttachType = false,
			SimplifyMaps = true,
			SimplifySets = true,
		};

		static readonly string MonospaceFontFamily = DeviceInfo.Platform.Is(DevicePlatform.Android) ? "monospace" : "Courier";

		public async void Expose()
		{
			if (ActiveFontInfo.IsNot()) ActiveColorPalette = "Ocean";
			ResetLocalization();

			FontFamilies ??= LoadFontFamilies();
			FontInfos ??= new FontInfo[]
			{
				new("Basic"    , 13.8, DefaultFontFamily  , FontAttributes.Bold),
				new("Hours"    , 20.0, MonospaceFontFamily, FontAttributes.Bold),
				new("Minutes"  , 13.2, MonospaceFontFamily),
				new("Tab"      , 17.0, DefaultFontFamily),
				new("ActiveTab", 23.0, DefaultFontFamily)
			};

			FontInfos[0].Size = 13.8;
			FontInfos[1].Size = 20.0;
			FontInfos[2].Size = 13.2;
			FontInfos[3].Size = 17.0;
			FontInfos[4].Size = 23.0;

			ActiveFontInfo ??= FontInfos[0];

			var skipUIRefresh = false;
			this[() => UseTransliteration].Changed += async args =>
			{
				if (skipUIRefresh.Is(true)) return;
				await Task.Delay(512); // use the delay to avoid UI refresh lacks
				ForceUIRefresh_Hack();
			};

			this[() => ActiveLanguage].Changed += async args =>
			{
				await Task.Delay(128);

				skipUIRefresh = true;
				UseTransliteration = ActiveLanguage.Is(English);
				skipUIRefresh = false;

				LocalizationSource.Wrap.ActiveManager = new LanguageManager(ActiveLanguage);
				ForceUIRefresh_Hack();

				if (LanguageCodeToLinks.Is())
				{
					ActiveLinks = LanguageCodeToLinks.GetValue(ActiveLanguage.ToString(),
						fallbackValue: LanguageCodeToLinks.FirstOrDefault().Value);
					EvokePropertyChanged(nameof(ActiveLinks));
				}
				else await RefreshLinks();
			};
#if !WPF
			var counter = 0;
			this[() => ActiveColorPalette].Changed += async args =>
			{
				++counter;
				await Task.Delay(64);
				if (--counter > 0) return;

				Application.Current.To<App>().ColorPalette = Palettes.Colors.Create(ActiveColorPalette);

				Palette.Colors.Refresh();
				ForceUIRefresh_Hack();
			};

			this[() => AllowAdvertising].Changed += args =>
				YandexMetrica.Report(nameof(AllowAdvertising), AllowAdvertising.ToString());

			var keys = Palettes.Colors.KeyToColors.Keys;
			ColorPalettes = keys.ToSet();
			this[Context.Get("Donate")].Executed += async args =>
			{
				var defaultDonationLink = "https://www.paypal.com/donate/?hosted_button_id=S8LGP5BQ5WA8Q";
				var donationLink = ActiveLinks.FirstOrDefault(l => l.Text.Contains("Donate".Localize())).Address
					?? defaultDonationLink;

				await Browser.OpenAsync(donationLink);
				AllowBonus = true;

				YandexMetrica.Report("Donate", donationLink);
			};

			this[Context.Get("Rate")].Executed += async args =>
			{
				await RateStatus.OpenAppStoreLink();
				
				await Task.Delay(512);

				App.RateStatus = DateTime.Now.ToString(CultureInfo.InvariantCulture);
				Context.Get("AllowBonus").EvokeCanExecuteChanged();
				AllowBonus = true;

				YandexMetrica.Report("Rate");
			};
#endif

			this[Context.Get("Navigate")].Executed += async args =>
			{
				try
				{
#if WPF
					var uri = args.Parameter?.ToString();
					await System.Diagnostics.Process.Start(uri).ToAsync();
#else
					var uri = args.Parameter?.ToString().Format(AppInfo.PackageName);
					if (uri.Is("RefreshLinks"))
						await RefreshLinks();
					else
					{
						await Browser.OpenAsync(uri);

						YandexMetrica.Report("Navigate", uri);
					}
#endif
				}
				catch (Exception exception)
				{
					YandexMetrica.ReportError(exception.Message, exception);
				}
			};

			await WebSyncer.Default.TrimByAge(64, "http://kogda.by");
			await WebSyncer.Default.TrimByAge(64, "http://www.m.gortransperm.ru");

			await WebSyncer.Default.TrimByAge(64, "https://kogda.by");
			await WebSyncer.Default.TrimByAge(64, "https://www.m.gortransperm.ru");
			await WebSyncer.Default.TrimByAge(64, "https://transport.mos.ru");
		}

		public async Task RefreshLinks()
		{
			try
			{
				var settingsFilePath = await FileSyncer.Default.GetPath("Settings.txt");
				var linksData = await Task.Run(() => File.ReadAllText(settingsFilePath));
				LanguageCodeToLinks = linksData.ParseSnapshot().ReplicateGraph<Dictionary<string, Link[]>>(_replicationProfile);
				ActiveLinks = LanguageCodeToLinks.GetValue(ActiveLanguage.ToString(),
					fallbackValue: LanguageCodeToLinks.FirstOrDefault().Value);
				EvokePropertyChanged(nameof(ActiveLinks));
			}
			catch (Exception exception)
			{
				YandexMetrica.ReportError(exception.Message, exception);
			}
		}

		public const string DefaultFontFamily = "~ system ~";
		public static SmartSet<string> LoadFontFamilies() => new SmartSet<string>()
			.Merge(DefaultFontFamily)
			.AppendRange(Aides.SystemFontFamiliesProvider?.Invoke() ?? new string[0]);

		public static LanguageCodes GetDefaultLanguage() =>
			Thread.CurrentThread.CurrentUICulture.Name.Is(out var cultureName) &&
			cultureName.StartsWith("ru") ? Russian :
			cultureName.StartsWith("uk") ? Ukrainian :
			cultureName.StartsWith("be") ? Belorussian :
			English;

		public void ResetLocalization()
		{
			LocalizationSource.Wrap.ActiveManager = default;
			LocalizationSource.Wrap.ActiveManager = new LanguageManager(ActiveLanguage);
		}

		static int counter = 0;
		public static async void ForceUIRefresh_Hack()
		{
			++counter;
			await Task.Delay(64);
			if (--counter > 0) return;

			var timetableViewModel = Store.Get<ViewModels.TimetableViewModel>();
			var root = timetableViewModel.Root;

			timetableViewModel.Root = default;
			timetableViewModel.EvokePropertyChanged(nameof(timetableViewModel.Root));

			timetableViewModel.Root = root;
			timetableViewModel.EvokePropertyChanged(nameof(timetableViewModel.Root));

			// for full redraw
			App.RootView?.Use(async v =>
			{
				v.IsEnabled.To(out var isEnabled);
				v.IsEnabled = isEnabled.Not();

				await Task.Delay(32);

				v.IsEnabled = isEnabled;
			});
		}
	}
}
