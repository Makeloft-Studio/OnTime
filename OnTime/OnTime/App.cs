﻿using Ace;
using Ace.Dictionaries;

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Yandex.Metrica;

using OnTime.Extensions;
using OnTime.Palettes;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace OnTime
{
	public partial class App : Application
	{
		public static AppView RootView;

		public static readonly string AppDataFolderPath =
			Environment.SpecialFolder.ApplicationData.GetPath();

		static App()
		{
			Store.ActiveBox.KeyFormat = Path.Combine(AppDataFolderPath, "{0}.json");

			var originalTimestamp = DateTime.Now;
			Store.Get<AppViewModel>().PropertyChanged += async (o, e) =>
			{
				var activeTimestamp = DateTime.Now;
				originalTimestamp = activeTimestamp;
				await Task.Delay(2048);
				if (activeTimestamp.IsNot(originalTimestamp)) return;
				Store.Snapshot();
			};
		}

		void LoadResources() => Resources.MergedDictionaries
			.Use(r => r.Add(new AppConverters()))
			.Use(r => r.Add(new Colors()))
			.Use(r => r.Add(new Palettes.Converters()))
			.Use(r => r.Add(new Sets()))
			.Use(r => r.Add(new Styles()))
			.Use(r => r.Add(new Templates()))
			;

		public App()
		{
			LoadResources();

			MainPage = RootView = new AppView();

			var appViewModel = Store.Get<AppViewModel>();
			ColorPalette = Palettes.Colors.Create(appViewModel.ActiveColorPalette);
			appViewModel.FontInfos.ForEach(i => i.Apply());

			Hack(appViewModel.FontInfos[0]);
			FontInfo.Refresh();
		}

		public static TResource Get<TResource>([CallerMemberName] string key = default) => (TResource)Current.Resources[key];

		static Style _itemLabelStyle;
		public static Style ItemLabelStyle => _itemLabelStyle ??= Get<Style>();

		static IValueConverter _sourceStatusToColorConverter, _sourceStatusToTextConverter, _textLengthToFontSizeConverter, _translitConverter;

		public static IValueConverter SourceStatusToColorConverter => _sourceStatusToColorConverter ??= Get<IValueConverter>();
		public static IValueConverter SourceStatusToTextConverter => _sourceStatusToTextConverter ??= Get<IValueConverter>();
		public static IValueConverter TextLengthToFontSizeConverter => _textLengthToFontSizeConverter ??= Get<IValueConverter>();
		public static IValueConverter TranslitConverter => _translitConverter ??= Get<IValueConverter>();

		public async void Hack(FontInfo basic)
		{
			var attributes = basic.Attributes;
			basic.Attributes = default;
			await Task.Delay(512);
			basic.Attributes = attributes;
		}

		public ResourceDictionary ColorPalette
		{
			get => Resources.MergedDictionaries.To<ObservableCollection<ResourceDictionary>>()[1];
			set => Resources.MergedDictionaries.To<ObservableCollection<ResourceDictionary>>()[1] = value;
		}

		public static string UserGuid
		{
			get => Get(File.ReadAllText);
			set => Set(File.WriteAllText, value);
		}

		public static string RateStatus
		{
			get => Get(File.ReadAllText);
			set => Set(File.WriteAllText, value);
		}

		public static uint WakeCounter
		{
			get => BytesToUInt32(Get(File.ReadAllBytes));
			set => Set(File.WriteAllBytes, UInt32ToBytes(value));
		}

		static byte[] UInt32ToBytes(uint value) => BitConverter.GetBytes(value);
		static uint BytesToUInt32(byte[] value) => value.Is(out var bytes) && bytes.Length.Is(4)
			? BitConverter.ToUInt32(bytes, 0)
			: 0;

		static T Get<T>(Func<string, T> reader, [CallerMemberName] string key = default) =>
			Try.Invoke(() => reader(Path.Combine(AppDataFolderPath, key)), out var value)
				? value
				: default;

		static T Set<T>(Action<string, T> writer, T value, [CallerMemberName] string key = default) =>
			Try.Invoke(() => writer(Path.Combine(AppDataFolderPath, key), value))
				? value
				: default;

		private async void OnWake()
		{
			var (userGuid, wakeCounter, rateStatus) = (UserGuid, WakeCounter, RateStatus);
			if (UserGuid.IsNot())
				UserGuid = userGuid = Guid.NewGuid().ToString();

			wakeCounter++;
			WakeCounter = wakeCounter;

			YandexMetrica.Report("Wake Chart", wakeCounter.ToString(), userGuid);

			var actualStatus = await OnTime.RateStatus.UpdateRateStatus(rateStatus, wakeCounter);
			if (rateStatus.IsNot(actualStatus))
				RateStatus = actualStatus;
		}

		protected override async void OnStart()
		{
			YandexMetrica.Wake();
			await Task.Factory.StartNew(OnWake);
		}

		protected override void OnSleep()
		{
			Store.Snapshot();
			YandexMetrica.Lull();
		}

		protected override async void OnResume()
		{
			YandexMetrica.Wake();
			await Task.Factory.StartNew(OnWake);
		}
	}
}
