﻿using Ace;
using Ace.Controls;

using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

using static OnTime.Views.TimetableView;

namespace OnTime.Extensions
{
	static class EventsLineRender
	{
		public struct Text
		{
			public Thickness Margin { get; set; }
			public double FontSize { get; set; }
			public string FontFamily { get; set; }
			public double FontScale { get; set; }
			public FontAttributes FontAttributes { get; set; }
			public double ScaledFontSize => FontSize * FontScale;
		}

		public struct Options
		{
			public Color AccentColor => (Color)App.Current.Resources["Accent"];
			public Color RegularColor => (Color)App.Current.Resources["AccentTextColor"];
			public Color OpacityColor => (Color)App.Current.Resources["BasicTextColor"];

			public Text Minutes { get; set; }
			public Text Hours { get; set; }
		}

		public static View Construct(DateTime timestamp, int hour, Event[] events, Event upcomingEvent, Options options)
		{
			var eventsPresentation = events.Aggregate("", (s, e) => 
			s += e.Equals(upcomingEvent)
				? $"[{e.Value.Minute:D2}]"
				: $" {e.Value.Minute:D2} ");

			var fontSize = eventsPresentation.Length > 56 * options.Minutes.FontScale
				? options.Minutes.FontSize / 1.5
				: options.Minutes.FontSize;

			var scaledFontSize = fontSize * options.Minutes.FontScale;
			var isDone = events.All(e => e.Value < timestamp);
			var isActive = upcomingEvent.Is() && upcomingEvent.Value.Hour.Is(hour);
			var eventsBlock = new Rack { Margin = options.Minutes.Margin };

			GetTextBasedMinuteRow(eventsPresentation, isDone, options)
				.Select(l => l.With
				(
					l.FontSize = scaledFontSize,
					l.FontFamily = options.Minutes.FontFamily,
					l.FontAttributes = options.Minutes.FontAttributes,
					l.VerticalOptions = LayoutOptions.Start
				))
				.ForEach(eventsBlock.Children.Add);

			var grid = new Rack
			{
				Children =
				{
					new Label
					{
						Text = hour.ToString("D2"),
						Margin = options.Hours.Margin,
						FontSize = options.Hours.ScaledFontSize,
						FontFamily = options.Hours.FontFamily,
						FontAttributes = options.Hours.FontAttributes,
						TextColor = isActive ? options.AccentColor : isDone ? options.OpacityColor : options.RegularColor
					}.Use(v => Rack.SetCell(v, "C0")),
					eventsBlock.Use(v => Rack.SetCell(v, "C1"))
				}
			}.Use(v => Rack.SetColumns(v, "^ *"));

			return grid;
		}

		private static IEnumerable<Label> GetTextBasedMinuteRow(string eventsRow, bool isDone, Options options)
		{
			var from = eventsRow.IndexOf("[");
			var till = eventsRow.IndexOf("]");
			if (from < 0)
			{
				yield return CreateLabel(eventsRow, isDone ? options.OpacityColor : options.RegularColor);
				yield break;
			}

			/* '\u2007' FIGURE SPACE */
			var count = from > 0 ? eventsRow.Substring(0, from).Count(c => c.Is(' ')) / 2 : 0;
			var whiteSpaceWithValue = Enumerable.Repeat(" \u2007\u2007 ", count + 1).Concat();
			var whiteSpaceBeforeValue = Enumerable.Repeat(" \u2007\u2007 ", count).Concat();
			var underlinedSpaceOfValue = new string(Enumerable.Repeat('_', till - from).ToArray());

			var spacedMinutesAfterValue = whiteSpaceWithValue + eventsRow.Substring(till + 1);
			var minutesBeforeValue = eventsRow.Substring(0, from);
			var spacedValue = whiteSpaceBeforeValue + eventsRow.Substring(from, till - from).Replace("[", "•").Replace("]", " ");
			var spacedValueUnerline = whiteSpaceBeforeValue + underlinedSpaceOfValue;

			yield return CreateLabel(spacedMinutesAfterValue, options.RegularColor);
			yield return CreateLabel(spacedValueUnerline, options.AccentColor);
			yield return CreateLabel(spacedValue, options.AccentColor);
			yield return CreateLabel(minutesBeforeValue, options.OpacityColor);
		}

		private static Label CreateLabel(string text, Color color) => new()
		{
			LineBreakMode = LineBreakMode.CharacterWrap,
			TextColor = color,
			Text = text
		};
	}
}
