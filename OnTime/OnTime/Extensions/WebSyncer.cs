﻿using Ace;

using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Yandex.Metrica;

using Xamarin.Essentials;

using static System.Globalization.CultureInfo;

using UriTable = System.Collections.Generic.Dictionary
	<string, (bool SkipEscape, System.DateTime Timestamp, int Hits, string FileName)>;

namespace OnTime.Extensions
{
	public class WebSyncer
	{
		/* allow Uri 'Skip Escape' option */
		static WebSyncer() => typeof(Uri).SetNonPublicStaticField("s_IriParsing", false); // s_IdnScope = 2||0

		public enum Modes
		{
			Default, ForceSync, SilentRepeat
		}

		public static WebSyncer Default { get; set; } =
			new(Environment.SpecialFolder.ApplicationData.GetPath());

		private readonly UriTable UriTable = new();

		public Modes Mode { get; set; }
		public string UriTablePath { get; }
		public string SnapshotFolderPath { get; }

		public WebSyncer(string folderPath)
		{
			UriTablePath = Path.Combine(folderPath, "UriTable");
			SnapshotFolderPath = Path.Combine(folderPath, "data");

			try
			{
				Directory.CreateDirectory(folderPath);
				Directory.CreateDirectory(SnapshotFolderPath);

				if (File.Exists(UriTablePath))
				{
					var snapshot = ReadAllText(UriTablePath);
					UriTable = Read(snapshot);
				}
			}
			catch
			{
			}
		}

		static UriTable Read(string snapshot) => snapshot
			.SplitByChars("\n")
			.Select(l => l.SplitByChars("\t"))
			.ToDictionary(row => row[0], p => (p[1].Is("*"), DateTime.Parse(p[2], InvariantCulture), int.Parse(p[3]), p[4]));

		static string ToEscapeMark(bool skipEscape) => skipEscape ? "*" : "^";

		static string Keep(UriTable table) => table
			.Aggregate("", (s, row) =>
				$"{s}{row.Key}\t{ToEscapeMark(row.Value.SkipEscape)}\t{row.Value.Timestamp.ToString(InvariantCulture)}\t{row.Value.Hits}\t{row.Value.FileName}\n");

		static async Task<string> ReadAllTextAsync(string path) =>
			await Task.Run(() => ReadAllText(path));

		static string ReadAllText(string path)
		{
			lock (path)
			{
				try
				{
					using var stream = new FileStream(path, FileMode.OpenOrCreate);
					using var archive = new ZipArchive(stream, ZipArchiveMode.Update);
					var name = Path.GetFileName(path);
					var entry = archive.GetEntry(name);
					using var reader = new StreamReader(entry.Open());
					return reader.ReadToEnd();
				}
				catch
				{
					return File.ReadAllText(path);
				}
			}
		}

		static async Task WriteAllTextAsync(string path, string text) =>
			await Task.Run(() => WriteAllText(path, text));

		static void WriteAllText(string path, string text)
		{
			lock (path)
			{
				try
				{
					using var stream = new FileStream(path, FileMode.Create);
					using var archive = new ZipArchive(stream, ZipArchiveMode.Update);
					var name = Path.GetFileName(path);
					var entry = archive.CreateEntry(name);
					using var writer = new StreamWriter(entry.Open());
					writer.Write(text);
				}
				catch
				{
					File.WriteAllText(path, text);
				}
			}
		}

		[Obsolete]
		public async Task<string> Load(string uriValue, bool skipEscape = false,
			Encoding encoding = default, bool allowAutoRedirect = false, HttpContext context = default)
		{
			var isUriHit = UriTable.TryGetValue(uriValue, out var value);
			var fileName = isUriHit ? value.FileName : Guid.NewGuid().ToString();
			var snapshotPath = Path.Combine(SnapshotFolderPath, fileName);

			static bool IsActual(DateTime timestamp) => (DateTime.Now - timestamp).TotalDays < 1;
#if WPF || PARSER
			var isReusing = isUriHit && File.Exists(snapshotPath) && IsActual(value.Timestamp);
			var isActual = isReusing;
#else
			static bool NetworkAceessIsNone() => Connectivity.NetworkAccess.Is(NetworkAccess.None);

			var isHit = isUriHit && File.Exists(snapshotPath);
			var isActual = isHit && IsActual(value.Timestamp);
			var networkAceessIsNone = NetworkAceessIsNone();
			var isReusing = networkAceessIsNone
				? isHit
				: (isActual && Mode.IsNot(Modes.ForceSync)) || (isHit && Mode.Is(Modes.SilentRepeat));

			if (networkAceessIsNone.Not() && Mode.Is(Modes.ForceSync))
				Mode = default;
			if (isReusing.Is(false) && Mode.Is(Modes.SilentRepeat))
				Mode = default;
#endif

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			var snapshot = isReusing
				? await ReadAllTextAsync(snapshotPath)
				: context.Is()
					? await uriValue.ToUri(skipEscape).DownloadStringTaskAsync(context.Client,
						context.GetBody.Is(out var body) ? await body() : default, encoding)
					: await uriValue.ToUri(skipEscape).DownloadStringTaskAsync(encoding, allowAutoRedirect);

			stopwatch.Stop();

			var key = isUriHit ? "Hits" : "Misses";
			var targetUriToMs = isActual ? "File (uri~ms)" : "Web (uri~ms)";
			var targetMsToUri = isActual ? "File (ms~uri)" : "Web (ms~uri)";
			var milliseconds = stopwatch.ElapsedMilliseconds.ToString();
			YandexMetrica.Report("Cache", key, uriValue);
			YandexMetrica.Report(targetUriToMs, uriValue, milliseconds);
			YandexMetrica.Report(targetMsToUri, milliseconds, uriValue);

			if (Mode.Is(Modes.SilentRepeat))
				return snapshot;

			try
			{
				if (isActual.Is(false))
				{
					await WriteAllTextAsync(snapshotPath, snapshot);
				}

				var hits = isUriHit ? value.Hits + 1 : 1;
				var timestamp = isActual ? value.Timestamp : DateTime.Now;
				UriTable[uriValue] = (skipEscape, timestamp, hits, fileName);

				var uriTableSnapshot = Keep(UriTable);
				await WriteAllTextAsync(UriTablePath, uriTableSnapshot);

				YandexMetrica.Report("Hits Chart", hits.ToString(), uriValue);
			}
			catch
			{
			}

			return snapshot;
		}

		public async Task TrimByAge(int days, params string[] dailyOrientedHosts)
		{
			var isUriTableActual = true;
			foreach (var row in UriTable.ToList())
			{
				var uriValue = row.Key;
				var (skipEscape, timestamp, hits, fileName) = row.Value;
				if ((DateTime.Now - timestamp).TotalDays < days) continue;
				try
				{
					var snapsotPath = Path.Combine(SnapshotFolderPath, fileName);
					File.Delete(snapsotPath);

					if (dailyOrientedHosts.Any(host => uriValue.StartsWith(host)))
					{
						UriTable.Remove(uriValue);
						isUriTableActual = false;
					}
				}
				catch
				{
					continue;
				}
			}

			if (isUriTableActual) return;
			var uriTableSnapshot = Keep(UriTable);
			await WriteAllTextAsync(UriTablePath, uriTableSnapshot);
		}
	}
}
