﻿using Ace;

using Xamarin.Forms;

namespace OnTime.Extensions
{
	static class Visual
	{
		public static void ResetValue(this VisualElement view, BindableProperty property, object stubValue = default)
		{
			var activeValue = view.GetValue(property);
			if (activeValue.Is(stubValue)) return;
			view.SetValue(property, stubValue);
			view.SetValue(property, activeValue);
		}

		public static Views.Picker ShiftSelection(this Views.Picker picker, int value)
		{
			var items = picker.ItemsSource;
			var activeItem = picker.SelectedItem;
			if (items.IsNot() || items.Count.Is(0)) return picker;

			var activeIndex = items.IndexOf(activeItem);
			var targetIndex = activeItem.Is()
				? activeIndex + value
				: value > 0 ? 0 : -1;

			var circleIndex = targetIndex % items.Count;
			var offsetIndex = circleIndex < 0 ? circleIndex + items.Count : circleIndex;
			picker.SelectedItem = items[offsetIndex];

			return picker;
		}
	}
}
