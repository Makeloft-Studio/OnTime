﻿using Ace;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OnTime.Extensions
{
	public class FileSyncer
    {
        public static FileSyncer Default { get; set; } =
            new(Environment.SpecialFolder.ApplicationData.GetPath());

        const string LinkFormat = "https://drive.google.com/uc?id={0}";
        const string SyncTableId = "1C_vAzO_yjlpF1VtZ9n6VqvHiWEMKr8A_";

        public static Dictionary<string, string> KeyToId = new()
        {
            ["transport.mos.ru.zip"] = "1Zt5yWmxb09zeG7VoYbfmoZYdEpftGrH3",
            ["Settings.txt"] = "1sTCvSLhv38WGJxOCPQoqO6ai7r7I7Akn",
        };

        string _folderPath;

        public FileSyncer(string folderPath)
		{
            _folderPath = folderPath;
        }

        private static Dictionary<string, int> ParseTable(string data) => data
            .SplitByChars("\n")
            .Select(l => l.Split('\t'))
            .ToDictionary(l => l[2].Trim(), l => int.Parse(l[1]));

        static async Task<string> ReadAllTextAsync(string path) =>
            await Task.Run(() => { lock (path) return File.ReadAllText(path); });

        static async Task WriteAllTextAsync(string path, string text) =>
            await Task.Run(() => { lock (path) File.WriteAllText(path, text); });

        static async Task WriteAllBytesAsync(string path, byte[] bytes) =>
            await Task.Run(() => { lock (path) File.WriteAllBytes(path, bytes); });

        public async Task<string> GetPath(string key)
		{
            var filePath = Path.Combine(_folderPath, key);

            try
			{
                var newSyncTableData = await LinkFormat.Format(SyncTableId).Load(allowAutoRedirect: true);

                var fileTablePath = Path.Combine(_folderPath, "FileTable");

                var oldSyncTableData = File.Exists(fileTablePath) ? await ReadAllTextAsync(fileTablePath) : "";

                var hasChanges = oldSyncTableData.IsNot(newSyncTableData);
                if (hasChanges)
                    await WriteAllTextAsync(fileTablePath, newSyncTableData);
                else if (File.Exists(filePath))
                    return filePath;

                var isActual =
                    File.Exists(filePath) &&
                    ParseTable(oldSyncTableData).TryGetValue(key, out var oldVersion) &&
                    ParseTable(newSyncTableData).TryGetValue(key, out var newVersion) &&
                    oldVersion == newVersion;

                if (isActual)
                {
                    return filePath;
                }
                else
                {
                    var id = KeyToId[key];
                    var fileBytes = await LinkFormat.Format(id).ToUri().DownloadByteArrayTaskAsync(allowAutoRedirect: true);
                    await WriteAllBytesAsync(filePath, fileBytes);
                    return filePath;
                }
            }
            catch (Exception exception)
			{
                if (File.Exists(filePath))
                    return filePath;

                throw exception;
            }
        }
    }
}
