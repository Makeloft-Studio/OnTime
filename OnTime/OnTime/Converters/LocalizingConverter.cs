﻿using Ace;
using Ace.Markup.Patterns;

namespace OnTime.Converters
{
	class LocalizingConverter : AValueConverter.Reflected
	{

		public override object Convert(object value) =>
			LocalizationSource.Wrap[value?.To<string>().Replace(" ", "").Replace("-", "")];
	}
}
