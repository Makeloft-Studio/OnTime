﻿using Xamarin.Forms;

namespace OnTime
{
	public static class Palette
	{
		public static class Colors
		{
			static Colors() => Refresh();

			public static Color SelectionColor { get; set; }

			public static void Refresh()
			{
				SelectionColor = (Color)App.Current.Resources["SelectionColor"];
			}
		}
	}
}
