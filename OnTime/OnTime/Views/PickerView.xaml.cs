﻿using Ace;
using OnTime.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.Specialized;
using Ace.Markup;

namespace OnTime.Views
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class PickerView
	{
		public TaskCompletionSource<object> SelectedItemSource { get; private set; } = new();

		public PickerView()
		{
			InitializeComponent();

			SelectionSet.SizeChanged += (o, e) => RefreshSelectionSet();

			BindingContextChanged += (o, e) =>
			{
				BindingContext.To(out _picker).With(_picker.ItemDisplayBinding.To(out _itemDisplayBinding));
				SelectionSet.ItemSelected += (s, args) => TrySetResult(args.SelectedItem);
				SearchEntry.Text = _picker.BindingContext.As(out PageViewModel p)?.SearchPattern;

				if (_picker.ItemsSource.Is(out _collection))
				{
					_collection.CollectionChanged += CollectionChanged;
				}
			};

			CloseButton.Clicked += (o, e) => TrySetResult(default, isCancel: true);

			SearchButton.Clicked += (o, e) => SearchEntry.Focus();
			CleanSearchButton.Clicked += (o, e) => SearchEntry.Text = default;

			SearchButton.IsVisible = SearchEntry.Text.IsNullOrEmpty();
			CleanSearchButton.IsVisible = SearchEntry.Text.IsNotNullOrEmpty();

			SearchEntry.TextChanged += (o, e) => SearchButton.IsVisible = SearchEntry.Text.IsNullOrEmpty();
			SearchEntry.TextChanged += (o, e) => CleanSearchButton.IsVisible = SearchEntry.Text.IsNotNullOrEmpty();
			SearchEntry.TextChanged += (o, e) => RefreshSelectionSet();
			SearchEntry.TextChanged += (o, e) => _picker.BindingContext.As(out PageViewModel p)?.With
			(
				p.SearchPattern = SearchEntry.Text
			);
		}

		Picker _picker = default;
		Binding _itemDisplayBinding = default;
		INotifyCollectionChanged _collection = default;

		void CollectionChanged(object s, NotifyCollectionChangedEventArgs a) =>
			RefreshSelectionSet(tryKeepScrollPosition: true);

		static bool Match(object item, string[] subpatterns, bool weakLastSubpattern, Binding itemDisplayBinding)
		{
			var displayValue = Picker.GetDisplayValue(item, itemDisplayBinding).To<string>();
			if (displayValue.IsNot())
				return false;

			var value = displayValue.ToLower();
			if (subpatterns.Any(subpattern => subpattern.Length > 2
					? value.Contains(subpattern)
					: value.Is(subpattern)))
				return true;
			return weakLastSubpattern && value.Contains(subpatterns.Last());
		}

		static IEnumerable<object> GetSearchState(IEnumerable<object> items, string pattern, Binding itemDisplayBinding) =>
			pattern.IsNullOrWhiteSpace() || items.IsNot() ||
			pattern.Trim().ToLower()
				.SplitByChars(" ,")
				.To(out var subpatterns).Length.Is(0)
				? items
				: items.Where(i => Match(i, subpatterns, char.IsLetterOrDigit(pattern.Last()), itemDisplayBinding));

		public async void RefreshSelectionSet(bool tryKeepScrollPosition = false)
		{
			double scrollX = 0d, scrollY = 0d;

			if (SelectionSet.Content.Is(out ScrollView scrollView) && tryKeepScrollPosition.Is(true))
			{
				scrollX = scrollView.ScrollX;
				scrollY = scrollView.ScrollY;
			}

			_ignoreSelection = true;
			SelectionSet.SelectedItem = default;

			var matchedItems = GetSearchState(_picker?.ItemsSource?.Cast<object>(), SearchEntry.Text, _itemDisplayBinding)?.ToList();
			var activeItem = _picker?.SelectedItem;

			SelectionSet.ItemsSource = SelectionSet.IsGroupingEnabled
				? matchedItems.GroupBy(i => Picker.GetDisplayValue(i, _itemDisplayBinding)).Cast<object>().ToList()
				: matchedItems;
			SelectionSet.SelectedItem = activeItem;
			_ignoreSelection = false;

			if (scrollView.Is() && tryKeepScrollPosition.Is(true))
			{
				await SelectionSet.TryScrollTo(scrollX, scrollY, animated: false);
			}
			else
			{
				await SelectionSet.TryScrollTo(activeItem);
			}
		}

		private bool _ignoreSelection = false;
		public bool TrySetResult(object result, bool isCancel = false)
		{
			if (_ignoreSelection.Is(true))
				return false;

			if (_collection.Is())
			{
				_collection.CollectionChanged -= CollectionChanged;
			}

			BindingContext.To(out Picker picker);

			var resetActiveItem = isCancel.Is(false) && picker.AllowActiveItemReset && result.Is(picker.SelectedItem);
			if (resetActiveItem)
			{
				if (picker.BindingContext.Is(out PageViewModel p))
					p.Set(() => p.ActiveItem, default);
				else
					picker.SelectedItem = default;
			}

			SelectedItemSource?.SetResult(resetActiveItem ? default : result);
			SelectedItemSource = default;
			return true;
		}

		public ContentView GetEmptyItemsContentView() => EmptyItemsContentView;

		public SetView GetSelectionSet() => SelectionSet;
	}
}
