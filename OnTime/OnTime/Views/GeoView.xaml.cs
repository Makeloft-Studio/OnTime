﻿using Ace;
using Ace.Markup.Converters;

using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;

using static Xamarin.Forms.WebNavigationResult;

namespace OnTime.Views
{
	public partial class GeoView
	{
		public GeoView()
		{
			InitializeComponent();

			PropertyChanged += BindWebViewSourceAtFirstTimeAppearance;
			void BindWebViewSourceAtFirstTimeAppearance(object sender, PropertyChangedEventArgs args)
			{
				if (nameof(IsVisible).IsNot(args.PropertyName) || IsVisible.Is(false))
					return;

				WebView.SetBinding(WebView.SourceProperty, new Binding
				{
					Path = "Root.ActiveItem.ActiveItem.ActiveItem",
					Converter = new Converter(args => GeoMaps.GetActiveUri())
				});

				PropertyChanged -= BindWebViewSourceAtFirstTimeAppearance;
			}

			WebView.Navigating += (o, e) => Show(ActivityIndicator);
			WebView.Navigated += (o, e) => Show(e.Result.Is(Success) ? WebView : RefreshButton);
			RefreshButton.Clicked += (o, e) => WebView.Reload();
			Show(RefreshButton);
			
			void Show(View view)
			{
				RefreshButton.IsVisible = false;
				ActivityIndicator.IsVisible = false;

				view.IsVisible = true;
			}

			WebView.Navigated += (o, e) => HideYandexBanner();
		}

		private async void HideYandexBanner()
		{
			// doesn't work
			//var cssInjectionScript = @"
			//var style = document.createElement('style')
			//style.innerText = '.n8a6c44d1 {display: none;}'
			//document.head.appendChild(style)
			//";

			//var result = await WebView.EvaluateJavaScriptAsync(cssInjectionScript);
			//return;

			var getTailChildrenHtml = "document.body.children[document.body.children.length - 1].innerHTML";
			var removeTailChildrenHtml = "document.body.removeChild(document.body.children[document.body.children.length - 1])";
			var yandexBannerSigns = new[]
			{
				"https://avatars",
				"Установить", "Install", "Усталяваць",
				"Открыть", "Open", "Адчыніць",
			};

			for (var i = 0; i < 32; i++)
			{
				var tailChildrenHtml = await WebView.EvaluateJavaScriptAsync(getTailChildrenHtml);
				var hasYandexBannerSign = tailChildrenHtml.Is() && yandexBannerSigns.Any(tailChildrenHtml.Contains);

				if (hasYandexBannerSign)
				{
					var result = await WebView.EvaluateJavaScriptAsync(removeTailChildrenHtml);
					if (result.Is()) break;
				}

				await Task.Delay(64);
			}
		}
	}
}
