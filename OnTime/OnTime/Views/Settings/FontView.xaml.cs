﻿using Ace;

using System.Threading.Tasks;

using Xamarin.Forms.Xaml;

namespace OnTime.Views.Settings
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class FontView
	{
		public FontView()
		{
			InitializeComponent();

			var appViewModel = Store.Get<AppViewModel>();
			appViewModel[() => appViewModel.ActiveLanguage].Changed += async args =>
			{
				await Task.Delay(512);

				ResetLocalizedValues(FontAttributesPicker);
			};
		}

		private void ResetLocalizedValues(Picker picker)
		{
			picker.ItemsSource.To(out var items);
			picker.SelectedItem.To(out var item);

			picker.ItemsSource = default;
			picker.SelectedItem = default;

			picker.ItemsSource = items;
			picker.SelectedItem = item;
		}
	}
}