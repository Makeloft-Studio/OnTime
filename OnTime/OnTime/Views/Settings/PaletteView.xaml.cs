﻿using Xamarin.Forms.Xaml;

namespace OnTime.Views.Settings
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class PaletteView
	{
		public PaletteView() => InitializeComponent();
	}
}