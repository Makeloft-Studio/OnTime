﻿using Ace;

using System.Threading.Tasks;

using OnTime.Extensions;

using Xamarin.Forms.Xaml;

namespace OnTime.Views
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class SettingsView
	{
		public SettingsView()
		{
			InitializeComponent();

			var appViewModel = Store.Get<AppViewModel>();
			appViewModel[() => appViewModel.ActiveLanguage].Changed += async args =>
			{
				await Task.Delay(512);

				this.ResetValue(BindingContextProperty);
			};

			appViewModel.RefreshLinks().RunAsync();
		}
	}
}
