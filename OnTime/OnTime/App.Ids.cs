﻿namespace OnTime
{
    partial class App
    {
		public const string AnaliticsKey =
#if DEBUG
			"933f644e-75eb-4ff8-863a-63123a82fe63";
#else
			"cfc0e6fb-08dc-44f8-aa65-652ea67d813d";
#endif

		public const string AdMobAppId =
			"ca-app-pub-3604001826685211~7333162900";
		//"ca-app-pub-3940256099942544~3347511713"; // test
		public const string AdMobPickerUnit =
			"ca-app-pub-3604001826685211/6465243296";
		//"ca-app-pub-3940256099942544/6300978111"; // test
	}
}
