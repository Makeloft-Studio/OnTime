﻿using Xamarin.Forms.Xaml;

namespace OnTime.Palettes
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class Styles
	{
		public Styles() => InitializeComponent();
	}
}
