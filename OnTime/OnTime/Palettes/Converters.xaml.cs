﻿using Ace.Markup.Patterns;

using Xamarin.Forms.Xaml;
using static System.DayOfWeek;

namespace OnTime.Palettes
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class Converters
	{
		public Converters() => InitializeComponent();

		private object DayToColor(ConvertArgs args) => args.Value?.ToString() switch
		{
			"Weekends" or nameof(Saturday) or nameof(Sunday) => App.Current.Resources["WeekendColor"],
			_ => App.Current.Resources["WorkdayColor"]
		};

		private object DiagnosticConvert(ConvertArgs args) =>
			args.Value;
	}
}
