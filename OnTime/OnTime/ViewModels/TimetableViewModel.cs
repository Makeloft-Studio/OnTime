﻿using Ace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnTime.Parsers.Patterns;

using static System.DayOfWeek;

namespace OnTime.ViewModels
{
	public class TimetableViewModel : ContextObject, IExposable
	{
		public (string Key, AParser[] Parsers, bool Status)[] Sources { get; set; }

		public PageViewModel Root { get; set; }

		public async void Expose()
		{
			var appViewModel = Store.Get<AppViewModel>();

			appViewModel[() => appViewModel.ActiveRegion].Changed += async args =>
				await LoadRoot(appViewModel.ActiveRegion);

			appViewModel.ActiveRegion ??= await GetDefaultRegion();

			if (Root.IsNot())
				await LoadRoot(appViewModel.ActiveRegion);
		}

		private async Task LoadRoot(string region)
		{
			var isManualRegionReset = Sources.Is();
			Sources = region.Is() ? Data.GetSources(region) : new (string Key, AParser[] Parsers, bool Status)[0];
			Root =
				new PageViewModel(default, "_", default, async rootPage => await Sources.Select(s =>
				new PageViewModel(rootPage, s.Parsers[0].GetSourceKey(), s.Key, async sourcePage => await s.Parsers.Select(p =>
				new PageViewModel(sourcePage, p.Kind, p.OriginalKind.To<string>(), async kindPage => await CreateProviders(p, kindPage))
				).ToAsync())
				{
					["Parsers"] = s.Parsers,
					["Status"] = s.Status,
				}
				).ToAsync());

			EvokePropertyChanged(nameof(Sources));
			EvokePropertyChanged(nameof(Root));

			await Root.Load();

			if (Root.ActiveItem.Is() || isManualRegionReset) return;

			try
			{
				Root.IsBusy = true;
				var defaultKey = await GetDefaultSourceKey();
				Root.ActiveItem = Root.Items.FirstOrDefault(p => p.Value.Is(defaultKey));
			}
			finally
			{
				Root.IsBusy = false;
			}
		}

		public async Task<string> GetDefaultRegion()
		{
			var appViewModel = Store.Get<AppViewModel>();
			var activeGeolocation = await Aides.GetActiveLocationOrDefault();
			if (activeGeolocation.Is())
			{
				var nearestSource = Geodata.Catalog
					.OrderBy(g => activeGeolocation.CalculateDistance(g.Value.Position))
					.FirstOrDefault()
					;
				var defaultRegionInfo = appViewModel.Regions
					.Select(r => (Key: r, Sources: Data.GetSources(r)))
					.FirstOrDefault(i => i.Sources.Any(s => s.Key.Is(nearestSource.Key)))
					;

				if (defaultRegionInfo.Key.Is(out var region))
					return region;
			}

			return appViewModel.Regions.FirstOrDefault();
		}

		private async Task<string> GetDefaultSourceKey()
		{
			if (Sources.Length.Is(1))
				return Sources.First().Key;

#if WPF
			var parts = "minsk".Split('.');
#else
			var parts = Xamarin.Essentials.AppInfo.PackageName.Split('.');
#endif
			if (Root.Items.FirstOrDefault(i => parts.Contains(i.Key)).Is(out var item))
			{
				return item.Key;
			}

			var activeGeolocation = await Aides.GetActiveLocationOrDefault();
			if (activeGeolocation.Is())
			{
				var (Key, Parsers, Status) = Sources.OrderBy(l =>
				{
					var a = Geodata.Catalog.TryGetValue(l.Key, out var data) ? data.Position : default;
					var b = (activeGeolocation.Latitude, activeGeolocation.Longitude);
					if (a.Is(default) || b.Is(default))
					{
						return double.PositiveInfinity;
					}

					var distance = Aides.CalculateDistance(a, b);
					return distance;
				}
				).First();
				return Key;
			}

			return default;
		}

		private static async Task<IEnumerable<PageViewModel>> CreateProviders(AParser parser, PageViewModel kindPage) =>
			(await parser.GetRoutes())
			.Select(route => new PageViewModel(kindPage, route.Key, route.Value, async routePage =>
			(await parser.GetDirections(route))
			.Select(direction => new PageViewModel(routePage, direction.Key, direction.Value, async directionPage =>
			(await parser.GetStops(route, direction))
			.Select(stop => new PageViewModel(directionPage, stop.Key, stop.Value, async stopPage =>
			(await parser.GetTimetables(route, direction, stop))
			.Select(timetable => new PageViewModel(stopPage, timetable.Key, timetable.Key, async timetablePage =>
			(await timetable.GetRows())
			.Select(row => new PageViewModel(timetablePage, row.Key, row.Value, default)))), ChooseDay
			))))));

		private static bool IsWeekend(DayOfWeek day) => day.Is(Saturday) || day.Is(Sunday);

		private static PageViewModel ChooseDay(IEnumerable<PageViewModel> items)
		{
			var day = Clock.GetTimetableDate().DayOfWeek;
			var dayRange = IsWeekend(day) ? "Weekends" : "Workdays";
			var dayName = day.ToString();
			var activeTimetable =
				items.FirstOrDefault(i => i.Key.Is(dayName)) ??
				items.FirstOrDefault(i => i.Key.Is(dayRange)) ??
				items.FirstOrDefault(i => i.Key.Is("Everyday")) ??
				items.Where(i => i.Key.IsNot("Workdays") && i.Key.IsNot("Weekends")).FirstOrDefault();

			return activeTimetable;
		}
	}
}
