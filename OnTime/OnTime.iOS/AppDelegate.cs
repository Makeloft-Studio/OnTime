﻿using Ace;

using Foundation;

using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;

using UIKit;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using Yandex.Metrica;

namespace OnTime.iOS
{
	[Register("AppDelegate")]
    public partial class AppDelegate : FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
			AppDomain.CurrentDomain.UnhandledException += (o, args) =>
			{
				for (var e = args.ExceptionObject as Exception; e.Is(); e = e.InnerException)
					YandexMetrica.ReportUnhandledException(e);

				//if (args.ExceptionObject is Exception exception)
				//	ShowExceptionAlert(exception);
			};

			try
			{
				RateStatus.AppStoreLink = "https://apps.apple.com/app/%D0%B2%D0%BE%D0%B2%D1%80%D0%B5%D0%BC%D1%8F/id1562113044";

				ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
				Aides.SystemFontFamiliesProvider = () => UIFont.FamilyNames;

				Forms.Init();

				ThreadPool.QueueUserWorkItem(RunAnalitics);

				LoadApplication(new App());

				if (Try.Invoke(SetupStatusBar, out var e).IsFalse())
					YandexMetrica.ReportError(e.Message, e);
			}
			catch (Exception exception)
			{
				//Log.Error("Loading Error", exception.ToString());
				for (var e = exception; e.Is(); e = e.InnerException)
					YandexMetrica.ReportError(e.Message, e);

				//ShowExceptionAlert(exception);
			}

            return base.FinishedLaunching(app, options);
        }

		[get: Export("advertisingIdentifier")]
		public virtual NSUuid AdvertisingIdentifier { get; }

		private void RunAnalitics(object o)
		{
			var (androidId, advertisingId) = default(string);

			try { androidId = default; }
			catch { }
			try { advertisingId = AdvertisingIdentifier.To<string>(); }
			catch { }
			try
			{
				var appDataFolderPath = Environment.SpecialFolder.ApplicationData.GetPath();
				var metricaFolderPath = Path.Combine(appDataFolderPath, "Yandex.Metrica");

				YandexMetricaFolder.SetCurrent(metricaFolderPath);
				YandexMetrica.Activate(App.AnaliticsKey, androidId, advertisingId);
			}
			catch (Exception exception)
			{
				//Log.Error("Yandex.Metrica", exception.ToString());
			}
		}

		private static UIView GetStatusBar()
		{
			if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
			{
				var tag = 128;
				var window = UIApplication.SharedApplication?.Windows?.FirstOrDefault();
				if (window.IsNot()) return default;

				var statusBar = window.ViewWithTag(tag);
				if (statusBar.IsNot())
				if (statusBar.IsNot())
				{
					statusBar = new UIView(UIApplication.SharedApplication.StatusBarFrame) { Tag = tag };
					window.AddSubview(statusBar);
				}

				return statusBar;
			}
			else 
				return UIApplication.SharedApplication.ValueForKey(new NSString("statusBar")) as UIView;
		}

		private static void SetupStatusBar()
		{
			var statusBar = GetStatusBar();
			if (statusBar.Is() && statusBar.RespondsToSelector(new ObjCRuntime.Selector("setBackgroundColor:")))
			{
				// plist   <key>UIViewControllerBasedStatusBarAppearance</key> <false/>
				UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.BlackOpaque;
				statusBar.BackgroundColor = Color.FromHex("#1976D2").ToUIColor(); // #7f6550
				statusBar.TintColor = Color.White.ToUIColor();
			}
		}
	}
}
