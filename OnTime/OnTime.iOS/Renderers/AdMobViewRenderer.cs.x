﻿
using Ace;

using CoreGraphics;

using Google.MobileAds;

using System;

using OnTime;
using OnTime.iOS;

using UIKit;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(AdMobView), typeof(AdMobViewRenderer))]
namespace OnTime.iOS
{
	public class AdMobViewRenderer : ViewRenderer<AdMobView, BannerView>
    {
        string bannerId = "AIzaSyCUba45AucavkynVoUPJmtR7nNNe-KG8o8";
        BannerView adView;
        BannerView CreateNativeAdControl()
        {
            if (adView != null)
                return adView;


            // Setup your BannerView, review AdSizeCons class for more Ad sizes. 
            adView = new BannerView(size: AdSizeCons.SmartBannerPortrait,
                                           origin: new CGPoint(0, UIScreen.MainScreen.Bounds.Size.Height - AdSizeCons.Banner.Size.Height))
            {
                AdUnitID = bannerId,
                //RootViewController = GetVisibleViewController()
            };

            // Wire AdReceived event to know when the Ad is ready to be displayed
            adView.AdReceived += (object sender, EventArgs e) =>
            {
                //ad has come in
            };


            adView.LoadRequest(GetRequest());
            return adView;
        }

        Request GetRequest()
        {
            var request = Request.GetDefaultRequest();
            // Requests test ads on devices you specify. Your test device ID is printed to the console when
            // an ad request is made. GADBannerView automatically returns test ads when running on a
            // simulator. After you get your device ID, add it here
            //request.TestDevices = new [] { Request.SimulatorId.ToString () };
            return request;
        }

        /// 
        /// Gets the visible view controller.
        /// 
        /// The visible view controller.
        UIViewController GetVisibleViewController()
        {
            try
			{
                if (UIApplication.SharedApplication.Is(out var app).Not())
                    return default;

                if (app.KeyWindow.Is(out var keyWindow).Not())
                    return default;

                if (keyWindow.RootViewController.Is(out var rootController).Not())
                    return default;

                if (rootController.PresentedViewController is null)
                    return rootController;

                if (rootController.PresentedViewController is UINavigationController)
                {
                    return ((UINavigationController)rootController.PresentedViewController).VisibleViewController;
                }

                if (rootController.PresentedViewController is UITabBarController)
                {
                    return ((UITabBarController)rootController.PresentedViewController).SelectedViewController;
                }

                return rootController.PresentedViewController;
            }
            catch (Exception e)
			{
                e = e;
                return default;
			}
        }

		protected override void OnElementChanged(ElementChangedEventArgs<AdMobView> e)
		{
            base.OnElementChanged(e);
            if (Control is null)
            {
                CreateNativeAdControl();
                SetNativeControl(adView);
            }
        }
	}
}