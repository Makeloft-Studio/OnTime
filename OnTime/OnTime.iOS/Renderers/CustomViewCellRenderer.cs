﻿using Timely.iOS.Renderers;

using UIKit;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ViewCell), typeof(CustomViewCellRenderer))]
namespace Timely.iOS.Renderers
{
	class CustomViewCellRenderer : ViewCellRenderer
	{
		public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tableView)
		{
			var cell = base.GetCell(item, reusableCell, tableView);
			
			cell.SelectedBackgroundView = new UIView
			{
				BackgroundColor = Palette.Colors.SelectionColor.ToUIColor(),
			};

			return cell;
		}
	}
}