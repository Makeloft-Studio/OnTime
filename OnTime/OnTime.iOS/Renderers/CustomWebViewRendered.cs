﻿using OnTime.iOS.Renderers;

using WebKit;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(WebView), typeof(CustomWebViewRenderer))]

namespace OnTime.iOS.Renderers
{
	public class CustomWebViewRenderer : WkWebViewRenderer
	{
		static WKWebViewConfiguration CreateConfiguration() => new WKWebViewConfiguration
		{
			Preferences = new WKPreferences
			{
				JavaScriptEnabled = true,
				JavaScriptCanOpenWindowsAutomatically = true
			},
		};

		public CustomWebViewRenderer() : base(CreateConfiguration()) { }
	}
}