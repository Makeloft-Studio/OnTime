﻿using UIKit;

namespace OnTime.iOS
{
    public class Application
    {
        // This is the main entry point of the application.
        static void Main(string[] args)
        {
            UILabel.Appearance.TintColor = UIColor.SystemPinkColor;
            UIButton.Appearance.TintColor = UIColor.SystemPinkColor;
            UIButton.Appearance.SetTitleColor(UIColor.LightGray, UIControlState.Normal);
            //UIListContentView.Appearance.TintColor = UIColor.SystemPinkColor;

            UIApplication.Main(args, null, "AppDelegate");
        }
    }
}
