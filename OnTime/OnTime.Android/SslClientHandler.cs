﻿using Android.Net;

using Javax.Net.Ssl;

using Xamarin.Android.Net;

namespace OnTime.Droid
{
	public class SslClientHandler : AndroidClientHandler
	{
		public class HostnameVerifier : Java.Lang.Object, IHostnameVerifier
		{
			public bool Verify(string hostname, ISSLSession session) => true;
		}

		protected override IHostnameVerifier GetSSLHostnameVerifier(HttpsURLConnection connection) =>
			new HostnameVerifier();

#pragma warning disable CS0618 // Type or member is obsolete
		protected override SSLSocketFactory ConfigureCustomSSLSocketFactory(HttpsURLConnection connection) =>
			SSLCertificateSocketFactory.GetInsecure(1000, null);
	}
}