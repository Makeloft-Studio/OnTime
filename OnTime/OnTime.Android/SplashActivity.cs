﻿using Ace;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Ads.Identifier;
using Android.OS;
using Android.Runtime;
using Android.Util;

using System;
using System.IO;
using System.Threading;

using Xamarin.Essentials;

using Yandex.Metrica;

namespace OnTime.Droid
{
	[Activity(
		Label = "в🕑время",
		Icon = "@drawable/Logo",
		Theme = "@style/SplashTheme",
		MainLauncher = true,
		ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation
		)]
	public class SplashActivity : Activity
	{
		static SplashActivity()
		{
			AppDomain.CurrentDomain.UnhandledException += (o, args) =>
			{
				for (var e = args.ExceptionObject as Exception; e is object; e = e.InnerException)
					Log.Error("Unhandled", e.ToString());
			};
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
		{
			Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
			base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}

		protected override void OnCreate(Bundle bundle)
		{
			AndroidEnvironment.UnhandledExceptionRaiser += (o, args) => ProcessUnhandledException(args.Exception);
			AppDomain.CurrentDomain.UnhandledException += (o, args) => ProcessUnhandledException(args.ExceptionObject as Exception);
			async void ProcessUnhandledException(Exception exception)
			{
				if (exception.IsNot())
					return;

				for (var e = exception; e.Is(); e = e.InnerException)
					YandexMetrica.ReportUnhandledException(e);

				await this.ShowAlertDialogAsync
				(
					"Oops".Localize() + "!",
					exception.Message,
					neutral: "Ok".Localize()
				);
			}

			base.OnCreate(bundle);

			ThreadPool.QueueUserWorkItem(RunAnalitics);

			Platform.Init(this, bundle);
			Xamarin.Forms.Forms.Init(this, bundle);

			StartActivity(new Intent(Application.Context, typeof(MainActivity)));
		}

		private void RunAnalitics(object o)
		{
			var (androidId, advertisingId) = default(string);

			try { androidId = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId); }
			catch { }
			try { advertisingId = AdvertisingIdClient.GetAdvertisingIdInfo(ApplicationContext).Id; }
			catch { }
			try
			{
				var appDataFolderPath = System.Environment.SpecialFolder.ApplicationData.GetPath();
				var metricaFolderPath = Path.Combine(appDataFolderPath, "Yandex.Metrica");

				YandexMetricaFolder.SetCurrent(metricaFolderPath);
				YandexMetrica.Activate(App.AnaliticsKey, androidId, advertisingId);
			}
			catch (Exception exception)
			{
				Log.Error("Yandex.Metrica", exception.ToString());
			}
		}
	}
}