﻿using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;

using OnTime;
using OnTime.Droid.Renderers;

using Android.Gms.Ads;

using Ace;
using Android.Views;

[assembly: ExportRenderer(typeof(AdMobView), typeof(AdMobViewRenderer))]

namespace OnTime.Droid.Renderers
{
	public class AdMobViewRenderer : ViewRenderer<AdMobView, AdView>
	{
		public AdMobViewRenderer(Android.Content.Context context) : base(context)
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<AdMobView> e)
		{
			base.OnElementChanged(e);

			if (Control is object)
				return;

			var control = e.NewElement;

			var adView = new AdView(Context)
			{
				Visibility = control.IsVisible ? ViewStates.Visible : ViewStates.Invisible,
				AdSize = AdSize.SmartBanner,
				AdUnitId = Element.AdUnitId,
			};
			
			SetNativeControl(adView);

			var requestCount = 0;
			if (adView.Visibility.Is(ViewStates.Visible))
			{
				requestCount++;
				adView.LoadAd(new AdRequest.Builder().Build());
			}

			control.PropertyChanged += (o, e) =>
			{
				if (nameof(control.IsVisible).IsNot(e.PropertyName))
					return;

				var isVisible = control.IsVisible;

				adView.Visibility = isVisible ? ViewStates.Visible : ViewStates.Invisible;

				if (requestCount.Is(0) && adView.Visibility.Is(ViewStates.Visible))
				{
					requestCount++;
					adView.LoadAd(new AdRequest.Builder().Build());
				}
			};
		}
	}
}