﻿using Ace;

using OnTime.Droid.Renderers;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(OnTime.Views.Picker), typeof(PickerViewRenderer))]

namespace OnTime.Droid.Renderers
{
	public class PickerViewRenderer : PickerRenderer, IPickerRenderer
	{
		public PickerViewRenderer(Android.Content.Context context) : base(context)
		{
		}

		void IPickerRenderer.OnClick() => Element.To<Views.Picker>().OnClick();
	}
}