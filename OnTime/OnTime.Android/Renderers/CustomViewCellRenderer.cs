﻿using Ace;

using System.ComponentModel;
using System.Linq;
using System.Reflection;

using Timely.Droid.Renderers;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ViewCell), typeof(CustomViewCellRenderer))]
namespace Timely.Droid.Renderers
{
    static class CellHelper
    {
        static readonly BindableProperty IsSelectedProperty;

        static CellHelper()
        {
            var type = typeof(Platform).Assembly.GetTypes().First(t => t.Name.Is("ListViewAdapter"));
            var flags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;
            var member = type.GetMember("IsSelectedProperty", MemberTypes.All, flags)[0];

            IsSelectedProperty = (BindableProperty)member.GetValue(new ViewCell());
        }

        public static bool GetIsSelected(this Cell cell) => (bool)cell.GetValue(IsSelectedProperty);
    }

	class CustomViewCellRenderer : ViewCellRenderer
	{
        static Color GetColor(bool isSelected) => isSelected ? Palette.Colors.SelectionColor : Color.Transparent;

        ViewCell _cell;

        protected override Android.Views.View GetCellCore(
            Cell item,
            Android.Views.View convertView,
            Android.Views.ViewGroup parent,
            Android.Content.Context context)
        {
            if (_cell.IsNot())
            {
                _cell = (ViewCell)item;
                _cell.View.BackgroundColor = GetColor(_cell.GetIsSelected());
            }

            var cellCore = base.GetCellCore(item, convertView, parent, context);
            //cellCore?.SetBackgroundColor(cellColor.ToAndroid());
            return cellCore;
        }

        protected override void OnCellPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnCellPropertyChanged(sender, e);

            if (e.PropertyName.Is("IsSelected"))
			{
                _cell.View.BackgroundColor = GetColor(_cell.GetIsSelected());
            }
		}
	}
}