﻿using Ace;
using System;

namespace OnTime.Converters
{
	class TimeRowToOpacityConverter : Ace.Markup.Patterns.AValueConverter
	{
		public override object Convert(object value)
		{
			value.To(out ViewModels.PageViewModel page);
			var now = DateTime.Now;
			var rowHour = int.Parse(page.Key);
			if (rowHour < now.Hour) return 0.4d;
			return 1d;
		}
	}
}