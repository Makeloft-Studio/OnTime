﻿using System;
using System.Threading.Tasks;

//namespace Xamarin.Essentials
//{
//	public class DisplayInfo
//	{
//		public double Density { get; set; }
//	}
//	public static class DeviceDisplay
//	{
//		public static DisplayInfo MainDisplayInfo { get; } = new DisplayInfo { Density = 1.0d };
//	}

//	//
//	// Summary:
//	//     Distance Unit for use in conversion.
//	//
//	// Remarks:
//	//     ///
//	//     ///
//	public enum DistanceUnits
//	{
//		//
//		// Summary:
//		//     Kilometers
//		Kilometers,
//		//
//		// Summary:
//		//     Miles
//		Miles
//	}

//	public class Location
//	{
//		//
//		// Summary:
//		//     Gets or sets the timestamp of the location.
//		public DateTimeOffset Timestamp
//		{
//			get;
//			set;
//		}

//		//
//		// Summary:
//		//     Gets or sets the latitude of location.
//		public double Latitude
//		{
//			get;
//			set;
//		}

//		//
//		// Summary:
//		//     Gets or sets the longitude of location.
//		public double Longitude
//		{
//			get;
//			set;
//		}

//		//
//		// Summary:
//		//     Gets the Altitude, if available in meters above sea level.
//		//
//		// Remarks:
//		//     Returns 0 or no value if not available.
//		public double? Altitude
//		{
//			get;
//			set;
//		}

//		//
//		// Summary:
//		//     Gets or sets the accuracy (in meters) of the location.
//		public double? Accuracy
//		{
//			get;
//			set;
//		}

//		//
//		// Summary:
//		//     Speed in meters per second.
//		public double? Speed
//		{
//			get;
//			set;
//		}

//		//
//		// Summary:
//		//     Degrees relative to true north.
//		//
//		// Remarks:
//		//     ///
//		//     ///
//		public double? Course
//		{
//			get;
//			set;
//		}

//		//
//		// Summary:
//		//     Inform if location is from GPS or from Mock.
//		//
//		// Remarks:
//		//     ///
//		//     ///
//		public bool IsFromMockProvider
//		{
//			get;
//			set;
//		}

//		public Location()
//		{
//		}

//		public Location(double latitude, double longitude)
//		{
//			Latitude = latitude;
//			Longitude = longitude;
//			Timestamp = DateTimeOffset.UtcNow;
//		}

//		public Location(double latitude, double longitude, DateTimeOffset timestamp)
//		{
//			Latitude = latitude;
//			Longitude = longitude;
//			Timestamp = timestamp;
//		}

//		public Location(Location point)
//		{
//			if (point == null)
//			{
//				throw new ArgumentNullException("point");
//			}
//			Latitude = point.Latitude;
//			Longitude = point.Longitude;
//			Timestamp = DateTime.UtcNow;
//			Speed = point.Speed;
//			Course = point.Course;
//		}

//		//
//		// Summary:
//		//     Calculate distance between two locations.
//		//
//		// Parameters:
//		//   latitudeStart:
//		//     Start latitude to calculate from.
//		//
//		//   longitudeStart:
//		//     Start longitude to calculate from.
//		//
//		//   locationEnd:
//		//     End location to calculate from.
//		//
//		//   units:
//		//     Unit to return.
//		//
//		// Returns:
//		//     Distance calculated.
//		public static double CalculateDistance(double latitudeStart, double longitudeStart, Location locationEnd, DistanceUnits units)
//		{
//			return CalculateDistance(latitudeStart, longitudeStart, locationEnd.Latitude, locationEnd.Longitude, units);
//		}

//		//
//		// Summary:
//		//     Calculate distance between two locations.
//		//
//		// Parameters:
//		//   locationStart:
//		//     Start location to calculate from.
//		//
//		//   latitudeEnd:
//		//     End latitude to calculate from.
//		//
//		//   longitudeEnd:
//		//     End longitude to calculate from.
//		//
//		//   units:
//		//     Unit to use.
//		//
//		// Returns:
//		//     Distance calculated.
//		public static double CalculateDistance(Location locationStart, double latitudeEnd, double longitudeEnd, DistanceUnits units)
//		{
//			return CalculateDistance(locationStart.Latitude, locationStart.Longitude, latitudeEnd, longitudeEnd, units);
//		}

//		//
//		// Summary:
//		//     Calculate distance between two locations.
//		//
//		// Parameters:
//		//   locationStart:
//		//     Start location to calculate from.
//		//
//		//   locationEnd:
//		//     End location to calculate from.
//		//
//		//   units:
//		//     Units to return.
//		//
//		// Returns:
//		//     Distance between two locations in the unit selected.
//		public static double CalculateDistance(Location locationStart, Location locationEnd, DistanceUnits units)
//		{
//			return CalculateDistance(locationStart.Latitude, locationStart.Longitude, locationEnd.Latitude, locationEnd.Longitude, units);
//		}

//		//
//		// Summary:
//		//     Calculate distance between two locations.
//		//
//		// Parameters:
//		//   latitudeStart:
//		//     Start latitude to calculate from.
//		//
//		//   latitudeEnd:
//		//     End latitude to calculate from.
//		//
//		//   longitudeStart:
//		//     Start longitude to calculate from.
//		//
//		//   longitudeEnd:
//		//     End longitude to calculate from.
//		//
//		//   units:
//		//     Units to return.
//		//
//		// Returns:
//		//     Distance between two locations in the unit selected.
//		public static double CalculateDistance(double latitudeStart, double longitudeStart, double latitudeEnd, double longitudeEnd, DistanceUnits units)
//		{
//			switch (units)
//			{
//				case DistanceUnits.Kilometers:
//					return UnitConverters.CoordinatesToKilometers(latitudeStart, longitudeStart, latitudeEnd, longitudeEnd);
//				case DistanceUnits.Miles:
//					return UnitConverters.CoordinatesToMiles(latitudeStart, longitudeStart, latitudeEnd, longitudeEnd);
//				default:
//					throw new ArgumentOutOfRangeException("units");
//			}
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public override string ToString()
//		{
//			return string.Format("{0}: {1}, {2}: {3}, ", "Latitude", Latitude, "Longitude", Longitude) + string.Format("{0}: {1}, {2}: {3}, ", "Altitude", Altitude ?? 0.0, "Accuracy", Accuracy ?? 0.0) + string.Format("{0}: {1}, {2}: {3}, ", "Speed", Speed ?? 0.0, "Course", Course ?? 0.0) + string.Format("{0}: {1}", "Timestamp", Timestamp);
//		}
//	}

//	public static class UnitConverters
//	{
//		private const double twoPi = Math.PI * 2.0;

//		private const double totalDegrees = 360.0;

//		private const double atmospherePascals = 101325.0;

//		private const double degreesToRadians = Math.PI / 180.0;

//		private const double milesToKilometers = 1.609344;

//		private const double milesToMeters = 1609.344;

//		private const double kilometersToMiles = 0.621371192237334;

//		private const double celsiusToKelvin = 273.15;

//		private const double meanEarthRadiusInKilometers = 6371.0;

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   fahrenheit:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double FahrenheitToCelsius(double fahrenheit)
//		{
//			return (fahrenheit - 32.0) / 1.8;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   celsius:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double CelsiusToFahrenheit(double celsius)
//		{
//			return celsius * 1.8 + 32.0;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   celsius:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double CelsiusToKelvin(double celsius)
//		{
//			return celsius + 273.15;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   kelvin:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double KelvinToCelsius(double kelvin)
//		{
//			return kelvin - 273.15;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   miles:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double MilesToMeters(double miles)
//		{
//			return miles * 1609.344;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   miles:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double MilesToKilometers(double miles)
//		{
//			return miles * 1.609344;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   kilometers:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double KilometersToMiles(double kilometers)
//		{
//			return kilometers * 0.621371192237334;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   degrees:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double DegreesToRadians(double degrees)
//		{
//			return degrees * (Math.PI / 180.0);
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   radians:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double RadiansToDegrees(double radians)
//		{
//			return radians / (Math.PI / 180.0);
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   degrees:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double DegreesPerSecondToRadiansPerSecond(double degrees)
//		{
//			return HertzToRadiansPerSecond(DegreesPerSecondToHertz(degrees));
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   radians:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double RadiansPerSecondToDegreesPerSecond(double radians)
//		{
//			return HertzToDegreesPerSecond(RadiansPerSecondToHertz(radians));
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   degrees:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double DegreesPerSecondToHertz(double degrees)
//		{
//			return degrees / 360.0;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   radians:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double RadiansPerSecondToHertz(double radians)
//		{
//			return radians / (Math.PI * 2.0);
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   hertz:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double HertzToDegreesPerSecond(double hertz)
//		{
//			return hertz * 360.0;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   hertz:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double HertzToRadiansPerSecond(double hertz)
//		{
//			return hertz * (Math.PI * 2.0);
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   kpa:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double KilopascalsToHectopascals(double kpa)
//		{
//			return kpa * 10.0;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   hpa:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double HectopascalsToKilopascals(double hpa)
//		{
//			return hpa / 10.0;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   kpa:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double KilopascalsToPascals(double kpa)
//		{
//			return kpa * 1000.0;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   hpa:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double HectopascalsToPascals(double hpa)
//		{
//			return hpa * 100.0;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   atm:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double AtmospheresToPascals(double atm)
//		{
//			return atm / 101325.0;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   pascals:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double PascalsToAtmospheres(double pascals)
//		{
//			return pascals * 101325.0;
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   lat1:
//		//     To be added.
//		//
//		//   lon1:
//		//     To be added.
//		//
//		//   lat2:
//		//     To be added.
//		//
//		//   lon2:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double CoordinatesToMiles(double lat1, double lon1, double lat2, double lon2)
//		{
//			return KilometersToMiles(CoordinatesToKilometers(lat1, lon1, lat2, lon2));
//		}

//		//
//		// Summary:
//		//     To be added.
//		//
//		// Parameters:
//		//   lat1:
//		//     To be added.
//		//
//		//   lon1:
//		//     To be added.
//		//
//		//   lat2:
//		//     To be added.
//		//
//		//   lon2:
//		//     To be added.
//		//
//		// Returns:
//		//     To be added.
//		//
//		// Remarks:
//		//     To be added.
//		public static double CoordinatesToKilometers(double lat1, double lon1, double lat2, double lon2)
//		{
//			if (lat1 == lat2 && lon1 == lon2)
//			{
//				return 0.0;
//			}
//			double num = DegreesToRadians(lat2 - lat1);
//			double num2 = DegreesToRadians(lon2 - lon1);
//			lat1 = DegreesToRadians(lat1);
//			lat2 = DegreesToRadians(lat2);
//			double num3 = Math.Sin(num / 2.0) * Math.Sin(num / 2.0);
//			double num4 = Math.Sin(num2 / 2.0) * Math.Sin(num2 / 2.0);
//			double d = num3 + num4 * Math.Cos(lat1) * Math.Cos(lat2);
//			double num5 = 2.0 * Math.Asin(Math.Sqrt(d));
//			return 6371.0 * num5;
//		}
//	}

//	//
//	// Summary:
//	//     Provides a way to get the current location of the device.
//	public static class Geolocation
//	{
//		//
//		// Summary:
//		//     Returns the last known location of the device.
//		//
//		// Returns:
//		//     Returns the location.
//		//
//		// Remarks:
//		//     This location may be a recently cached location.
//		public static Task<Location> GetLastKnownLocationAsync()
//		{
//			return PlatformLastKnownLocationAsync();
//		}

//		//
//		// Summary:
//		//     Returns the current location of the device.
//		//
//		// Returns:
//		//     Returns the location.
//		public static Task<Location> GetLocationAsync()
//		{
//			return PlatformLocationAsync(new GeolocationRequest(), default(CancellationToken));
//		}

//		//
//		// Summary:
//		//     Returns the current location of the device using the specified criteria.
//		//
//		// Parameters:
//		//   request:
//		//     The criteria to use when determining the location of the device.
//		//
//		// Returns:
//		//     Returns the location.
//		public static Task<Location> GetLocationAsync(GeolocationRequest request)
//		{
//			return PlatformLocationAsync(request ?? new GeolocationRequest(), default(CancellationToken));
//		}

//		//
//		// Summary:
//		//     Returns the current location of the device using the specified criteria.
//		//
//		// Parameters:
//		//   request:
//		//     The criteria to use when determining the location of the device.
//		//
//		//   cancelToken:
//		//     A token for cancelling the operation.
//		//
//		// Returns:
//		//     Returns the location.
//		public static Task<Location> GetLocationAsync(GeolocationRequest request, CancellationToken cancelToken)
//		{
//			return PlatformLocationAsync(request ?? new GeolocationRequest(), cancelToken);
//		}

//		private static Task<Location> PlatformLastKnownLocationAsync()
//		{
//			throw ExceptionUtils.NotSupportedOrImplementedException;
//		}

//		private static Task<Location> PlatformLocationAsync(GeolocationRequest request, CancellationToken cancellationToken)
//		{
//			throw ExceptionUtils.NotSupportedOrImplementedException;
//		}
//	}
//}
